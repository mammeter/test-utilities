-- Copyright 2016 Matthew Ammeter

-- This file is part of test-utilities.

-- test-utilities is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- test-utilities is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.

-- You should have received a copy of the GNU Lesser General Public License
-- along with test-utilities.  If not, see <http://www.gnu.org/licenses/>.

----------------------------------------------------------------------
-- Purpose:  Table to hold test data.
----------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS test.tst1_test1_tab(
    tst1_keyint NUMERIC(38, 0) NOT NULL,
    tst1_colvarchar VARCHAR(30) NULL,
    tst1_coldatetime TIMESTAMP NULL,
    tst1_lastupdateid VARCHAR(30) NOT NULL,
    tst1_lastupdatedate TIMESTAMP NOT NULL);

----------------------------------------------------------------------
-- Purpose:  The primary key constraint for the test 1 table.
----------------------------------------------------------------------
ALTER TABLE test.tst1_test1_tab
ADD CONSTRAINT tst1_pk
PRIMARY KEY(tst1_keyint);
