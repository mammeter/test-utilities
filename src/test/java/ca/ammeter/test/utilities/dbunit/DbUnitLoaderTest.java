/*
 * Copyright 2016 Matthew Ammeter
 *
 * This file is part of test-utilities.
 *
 * test-utilities is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * test-utilities is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * test-utilities. If not, see <http://www.gnu.org/licenses/>.
 */
package ca.ammeter.test.utilities.dbunit;

import java.math.BigDecimal;
import java.net.URL;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.sql.DataSource;

import com.google.common.io.Resources;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.operation.DatabaseOperation;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

/**
 * Tests that the {@link DbUnitLoader} correctly loads the database.
 *
 * @author Matthew Ammeter
 */
@ContextConfiguration(locations = "classpath:testing-context.xml")
@TestExecutionListeners({ TransactionalTestExecutionListener.class })
@Transactional(
    propagation = Propagation.REQUIRES_NEW,
    timeout = 300,
    transactionManager = "transactionManager")
public class DbUnitLoaderTest
        extends AbstractTestNGSpringContextTests {

    @Resource(name = "testDataSource")
    private DataSource dataSource;

    @Resource(name = "testDataSource2")
    private DataSource dataSource2;

    @Resource(name = "testJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    @Resource(name = "testJdbcTemplate2")
    private JdbcTemplate jdbcTemplate2;

    /**
     * Tests that calling {@link DbUnitLoader#createSingleDataSourceInstance(DataSource)}
     * with a null {@link DataSource} correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The data source cannot be null",
        groups = { "integration-tests" })
    public static void testCreateSingleDataSourceInstance_nullDataSource() {
        DbUnitLoader.createSingleDataSourceInstance(null);
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(String, String)} with a null
     * schema correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The schema cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_string_string_nullSchema() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            null,
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(String, String)} with a null file
     * path correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The file path cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_string_string_nullFilePathSchema() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            "schema",
            (String) null);
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(String, String)} with a
     * multi-datasource instance correctly throws an {@link IllegalStateException}.
     */
    @Test(
        expectedExceptions = IllegalStateException.class,
        expectedExceptionsMessageRegExp = "Must specify a data source in multi-datasource instance",
        groups = { "integration-tests" })
    public static void testAddDataSet_string_string_multiDataSourceInstance() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            "schema",
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(String, String)} with a
     * non-existent {@code filePath} correctly throws an {@link IllegalArgumentException}.
     */
    @Test(
        expectedExceptions = IllegalArgumentException.class,
        expectedExceptionsMessageRegExp = "resource garbage not found.",
        groups = { "integration-tests" })
    public void testAddDataSet_string_string_fileNotExists() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            "schema",
            "garbage");
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(String, String, Class)} with a
     * null schema correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The schema cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_string_string_class_nullSchema() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            null,
            "dataset1_without_replacements.xml",
            DbUnitLoaderTest.class);
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(String, String, Class)} with a
     * null file path correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The file path cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_string_string_class_nullFilePathSchema() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            "schema",
            (String) null,
            DbUnitLoaderTest.class);
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(String, String, Class)} with a
     * null relative class correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The relative class cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_string_string_class_nullRelativeClass() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            "schema",
            "dataset1_without_replacements.xml",
            null);
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(String, String, Class)} with a
     * multi-datasource instance correctly throws an {@link IllegalStateException}.
     */
    @Test(
        expectedExceptions = IllegalStateException.class,
        expectedExceptionsMessageRegExp = "Must specify a data source in multi-datasource instance",
        groups = { "integration-tests" })
    public static void testAddDataSet_string_string_class_multiDataSourceInstance() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            "schema",
            "dataset1_without_replacements.xml",
            DbUnitLoaderTest.class);
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(String, String, Class)} with a
     * non-existent {@code filePath} correctly throws an {@link IllegalArgumentException}.
     */
    @Test(
        expectedExceptions = IllegalArgumentException.class,
        expectedExceptionsMessageRegExp = "resource garbage relative to ca.ammeter.test.utilities.dbunit.DbUnitLoaderTest not found.",
        groups = { "integration-tests" })
    public void testAddDataSet_string_string_class_fileNotExists() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            "schema",
            "garbage",
            DbUnitLoaderTest.class);
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(String, URL)} with a null schema
     * correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The schema cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_string_url_nullSchema() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            null,
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(String, URL)} with a null
     * {@link URL} correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The URL cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_string_url_nullUrl() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            "schema",
            (URL) null);
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(String, URL)} with a
     * multi-datasource instance correctly throws an {@link IllegalStateException}.
     */
    @Test(
        expectedExceptions = IllegalStateException.class,
        expectedExceptionsMessageRegExp = "Must specify a data source in multi-datasource instance",
        groups = { "integration-tests" })
    public static void testAddDataSet_string_url_multiDataSourceInstance() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            "schema",
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DatabaseOperation, String, String)} with a null
     * database operation correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The database operation cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_databaseoperation_string_string_nullDatabaseOperation() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            (DatabaseOperation) null,
            "schema",
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DatabaseOperation, String, String)} with a null
     * schema correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The schema cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_databaseoperation_string_string_nullSchema() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            DatabaseOperation.REFRESH,
            null,
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DatabaseOperation, String, String)} with a null file
     * path correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The file path cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_databaseoperation_string_string_nullFilePathSchema() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            DatabaseOperation.REFRESH,
            "schema",
            (String) null);
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DatabaseOperation, String, String)} with a
     * multi-datasource instance correctly throws an {@link IllegalStateException}.
     */
    @Test(
        expectedExceptions = IllegalStateException.class,
        expectedExceptionsMessageRegExp = "Must specify a data source in multi-datasource instance",
        groups = { "integration-tests" })
    public static void testAddDataSet_databaseoperation_string_string_multiDataSourceInstance() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            DatabaseOperation.REFRESH,
            "schema",
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DatabaseOperation, String, String)} with a
     * non-existent {@code filePath} correctly throws an {@link IllegalArgumentException}.
     */
    @Test(
        expectedExceptions = IllegalArgumentException.class,
        expectedExceptionsMessageRegExp = "resource garbage not found.",
        groups = { "integration-tests" })
    public void testAddDataSet_databaseoperation_string_string_fileNotExists() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            DatabaseOperation.REFRESH,
            "schema",
            "garbage");
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DatabaseOperation, String, String, Class)} with a
     * null database operation correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The database operation cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_databaseoperation_string_string_class_nullDatabaseOperation() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            (DatabaseOperation) null,
            "schema",
            "dataset1_without_replacements.xml",
            DbUnitLoaderTest.class);
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DatabaseOperation, String, String, Class)} with a
     * null schema correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The schema cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_databaseoperation_string_string_class_nullSchema() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            DatabaseOperation.REFRESH,
            null,
            "dataset1_without_replacements.xml",
            DbUnitLoaderTest.class);
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DatabaseOperation, String, String, Class)} with a
     * null file path correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The file path cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_databaseoperation_string_string_class_nullFilePathSchema() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            DatabaseOperation.REFRESH,
            "schema",
            (String) null,
            DbUnitLoaderTest.class);
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DatabaseOperation, String, String, Class)} with a
     * null relative class correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The relative class cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_databaseoperation_string_string_class_nullRelativeClass() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            DatabaseOperation.REFRESH,
            "schema",
            "dataset1_without_replacements.xml",
            null);
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DatabaseOperation, String, String, Class)} with a
     * multi-datasource instance correctly throws an {@link IllegalStateException}.
     */
    @Test(
        expectedExceptions = IllegalStateException.class,
        expectedExceptionsMessageRegExp = "Must specify a data source in multi-datasource instance",
        groups = { "integration-tests" })
    public static void testAddDataSet_databaseoperation_string_string_class_multiDataSourceInstance() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            DatabaseOperation.REFRESH,
            "schema",
            "dataset1_without_replacements.xml",
            DbUnitLoaderTest.class);
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DatabaseOperation, String, String, Class)} with a
     * non-existent {@code filePath} correctly throws an {@link IllegalArgumentException}.
     */
    @Test(
        expectedExceptions = IllegalArgumentException.class,
        expectedExceptionsMessageRegExp = "resource garbage relative to ca.ammeter.test.utilities.dbunit.DbUnitLoaderTest not found.",
        groups = { "integration-tests" })
    public void testAddDataSet_databaseoperation_string_string_class_fileNotExists() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            DatabaseOperation.REFRESH,
            "schema",
            "garbage",
            DbUnitLoaderTest.class);
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(DatabaseOperation, String, URL)}
     * with a null database operation correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The database operation cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_databaseoperation_string_url_nullDatabaseOperation() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            (DatabaseOperation) null,
            "schema",
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(DatabaseOperation, String, URL)}
     * with a null schema correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The schema cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_databaseoperation_string_url_nullSchema() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            DatabaseOperation.REFRESH,
            null,
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(DatabaseOperation, String, URL)}
     * with a null {@link URL} correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The URL cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_databaseoperation_string_url_nullUrl() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            DatabaseOperation.REFRESH,
            "schema",
            (URL) null);
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(DatabaseOperation, String, URL)}
     * with a multi-datasource instance correctly throws an {@link IllegalStateException}.
     */
    @Test(
        expectedExceptions = IllegalStateException.class,
        expectedExceptionsMessageRegExp = "Must specify a data source in multi-datasource instance",
        groups = { "integration-tests" })
    public static void testAddDataSet_databaseoperation_string_url_multiDataSourceInstance() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            DatabaseOperation.REFRESH,
            "schema",
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(DataSource, String, String)} with
     * a null {@link DataSource} correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The data source cannot be null",
        groups = { "integration-tests" })
    public static void testAddDataSet_datasource_string_string_nullDataSource() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            (DataSource) null,
            "schema",
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(DataSource, String, String)} with
     * a null schema correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The schema cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_string_string_nullSchema() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            this.dataSource,
            null,
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(DataSource, String, String)} with
     * a null file path correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The file path cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_string_string_nullFilePathSchema() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            this.dataSource,
            "schema",
            (String) null);
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(DataSource, String, String)} on a
     * single-datasource instance with a second datasource passed correctly throws an
     * {@link IllegalStateException}.
     */
    @Test(
        expectedExceptions = IllegalStateException.class,
        expectedExceptionsMessageRegExp = "Cannot add second datasource to single-datasource instance",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_string_string_singleDataSourceInstance() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            this.dataSource2,
            "schema",
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(DataSource, String, String)} with
     * a non-existent {@code filePath} correctly throws an
     * {@link IllegalArgumentException}.
     */
    @Test(
        expectedExceptions = IllegalArgumentException.class,
        expectedExceptionsMessageRegExp = "resource garbage not found.",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_string_string_fileNotExists() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            this.dataSource,
            "schema",
            "garbage");
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, String, String, Class)} with a null
     * {@link DataSource} correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The data source cannot be null",
        groups = { "integration-tests" })
    public static void testAddDataSet_datasource_string_string_class_nullDataSource() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            (DataSource) null,
            "schema",
            "dataset1_without_replacements.xml",
            DbUnitLoaderTest.class);
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, String, String, Class)} with a null
     * schema correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The schema cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_string_string_class_nullSchema() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            this.dataSource,
            null,
            "dataset1_without_replacements.xml",
            DbUnitLoaderTest.class);
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, String, String, Class)} with a null file
     * path correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The file path cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_string_string_class_nullFilePathSchema() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            this.dataSource,
            "schema",
            (String) null,
            DbUnitLoaderTest.class);
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, String, String, Class)} with a null
     * relative class correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The relative class cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_string_string_class_nullRelativeClass() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            this.dataSource,
            "schema",
            "dataset1_without_replacements.xml",
            null);
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, String, String, Class)} on a
     * single-datasource instance with a second datasource passed correctly throws an
     * {@link IllegalStateException}.
     */
    @Test(
        expectedExceptions = IllegalStateException.class,
        expectedExceptionsMessageRegExp = "Cannot add second datasource to single-datasource instance",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_string_string_class_singleDataSourceInstance() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            this.dataSource2,
            "schema",
            "dataset1_without_replacements.xml",
            DbUnitLoaderTest.class);
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, String, String, Class)} with a
     * non-existent {@code filePath} correctly throws an {@link IllegalArgumentException}.
     */
    @Test(
        expectedExceptions = IllegalArgumentException.class,
        expectedExceptionsMessageRegExp = "resource garbage relative to ca.ammeter.test.utilities.dbunit.DbUnitLoaderTest not found.",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_string_string_class_fileNotExists() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            this.dataSource,
            "schema",
            "garbage",
            DbUnitLoaderTest.class);
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(DataSource, String, URL)} with a
     * null {@link DataSource} correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The data source cannot be null",
        groups = { "integration-tests" })
    public static void testAddDataSet_datasource_string_url_nullDataSource() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            (DataSource) null,
            "schema",
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(DataSource, String, URL)} with a
     * null schema correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The schema cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_string_url_nullSchema() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            this.dataSource,
            null,
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(DataSource, String, URL)} with a
     * null {@link URL} correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The URL cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_string_url_nullUrl() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            this.dataSource,
            "schema",
            (URL) null);
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(DataSource, String, URL)} on a
     * single-datasource instance with a second datasource passed correctly throws an
     * {@link IllegalStateException}.
     */
    @Test(
        expectedExceptions = IllegalStateException.class,
        expectedExceptionsMessageRegExp = "Cannot add second datasource to single-datasource instance",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_string_url_singleDataSourceInstance() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            this.dataSource2,
            "schema",
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, DatabaseOperation, String, String)} with
     * a null {@link DataSource} correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The data source cannot be null",
        groups = { "integration-tests" })
    public static void testAddDataSet_datasource_databaseoperation_string_string_nullDataSource() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            (DataSource) null,
            DatabaseOperation.REFRESH,
            "schema",
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, DatabaseOperation, String, String)} with
     * a null {@link DatabaseOperation} correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The database operation cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_databaseoperation_string_string_nullDatabaseOperation() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            this.dataSource,
            null,
            "schema",
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, DatabaseOperation, String, String)} with
     * a null schema correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The schema cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_databaseoperation_string_string_nullSchema() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            this.dataSource,
            DatabaseOperation.REFRESH,
            null,
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, DatabaseOperation, String, String)} with
     * a null file path correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The file path cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_databaseoperation_string_string_nullFilePathSchema() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            this.dataSource,
            DatabaseOperation.REFRESH,
            "schema",
            (String) null);
    }

    /**
     * Tests that calling {@link DbUnitLoader#addDataSet(DataSource, String, String)} on a
     * single-datasource instance with a second datasource passed correctly throws an
     * {@link IllegalStateException}.
     */
    @Test(
        expectedExceptions = IllegalStateException.class,
        expectedExceptionsMessageRegExp = "Cannot add second datasource to single-datasource instance",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_databaseoperation_string_string_singleDataSourceInstance() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            this.dataSource2,
            DatabaseOperation.REFRESH,
            "schema",
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, DatabaseOperation, String, String)} with
     * a non-existent {@code filePath} correctly throws an
     * {@link IllegalArgumentException}.
     */
    @Test(
        expectedExceptions = IllegalArgumentException.class,
        expectedExceptionsMessageRegExp = "resource garbage not found.",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_databaseoperation_string_string_fileNotExists() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            this.dataSource,
            DatabaseOperation.REFRESH,
            "schema",
            "garbage");
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, DatabaseOperation, String, String, Class)}
     * with a null {@link DataSource} correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The data source cannot be null",
        groups = { "integration-tests" })
    public static void testAddDataSet_datasource_databaseoperation_string_string_class_nullDataSource() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            (DataSource) null,
            DatabaseOperation.REFRESH,
            "schema",
            "dataset1_without_replacements.xml",
            DbUnitLoaderTest.class);
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, DatabaseOperation, String, String, Class)}
     * with a null {@link DatabaseOperation} correctly throws a
     * {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The database operation cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_databaseoperation_string_string_class_nullDatabaseOperation() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            this.dataSource,
            null,
            "schema",
            "dataset1_without_replacements.xml",
            DbUnitLoaderTest.class);
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, DatabaseOperation, String, String, Class)}
     * with a null schema correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The schema cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_databaseoperation_string_string_class_nullSchema() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            this.dataSource,
            DatabaseOperation.REFRESH,
            null,
            "dataset1_without_replacements.xml",
            DbUnitLoaderTest.class);
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, DatabaseOperation, String, String, Class)}
     * with a null file path correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The file path cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_databaseoperation_string_string_class_nullFilePathSchema() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            this.dataSource,
            DatabaseOperation.REFRESH,
            "schema",
            (String) null,
            DbUnitLoaderTest.class);
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, DatabaseOperation, String, String, Class)}
     * with a null relative class correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The relative class cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_databaseoperation_string_string_class_nullRelativeClass() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            this.dataSource,
            DatabaseOperation.REFRESH,
            "schema",
            "dataset1_without_replacements.xml",
            null);
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, DatabaseOperation, String, String, Class)}
     * on a single-datasource instance with a second datasource passed correctly throws an
     * {@link IllegalStateException}.
     */
    @Test(
        expectedExceptions = IllegalStateException.class,
        expectedExceptionsMessageRegExp = "Cannot add second datasource to single-datasource instance",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_databaseoperation_string_string_class_singleDataSourceInstance() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            this.dataSource2,
            DatabaseOperation.REFRESH,
            "schema",
            "dataset1_without_replacements.xml",
            DbUnitLoaderTest.class);
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, DatabaseOperation, String, String, Class)}
     * with a non-existent {@code filePath} correctly throws an
     * {@link IllegalArgumentException}.
     */
    @Test(
        expectedExceptions = IllegalArgumentException.class,
        expectedExceptionsMessageRegExp = "resource garbage relative to ca.ammeter.test.utilities.dbunit.DbUnitLoaderTest not found.",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_databaseoperation_string_string_class_fileNotExists() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            this.dataSource,
            DatabaseOperation.REFRESH,
            "schema",
            "garbage",
            DbUnitLoaderTest.class);
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, DatabaseOperation, String, URL)} with a
     * null {@link DataSource} correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The data source cannot be null",
        groups = { "integration-tests" })
    public static void testAddDataSet_datasource_databaseoperation_string_url_nullDataSource() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            (DataSource) null,
            DatabaseOperation.REFRESH,
            "schema",
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, DatabaseOperation, String, URL)} with a
     * null {@link DatabaseOperation} correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The database operation cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_databaseoperation_string_url_nullDatabaseOperation() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            this.dataSource,
            null,
            "schema",
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, DatabaseOperation, String, URL)} with a
     * null schema correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The schema cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_databaseoperation_string_url_nullSchema() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            this.dataSource,
            DatabaseOperation.REFRESH,
            null,
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, DatabaseOperation, String, URL)} with a
     * null {@link URL} correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The URL cannot be null",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_databaseoperation_string_url_nullUrl() {
        DbUnitLoader.createMultiDataSourceInstance().addDataSet(
            this.dataSource,
            DatabaseOperation.REFRESH,
            "schema",
            (URL) null);
    }

    /**
     * Tests that calling
     * {@link DbUnitLoader#addDataSet(DataSource, DatabaseOperation, String, URL)} on a
     * single-datasource instance with a second datasource passed correctly throws an
     * {@link IllegalStateException}.
     */
    @Test(
        expectedExceptions = IllegalStateException.class,
        expectedExceptionsMessageRegExp = "Cannot add second datasource to single-datasource instance",
        groups = { "integration-tests" })
    public void testAddDataSet_datasource_databaseoperation_string_url_singleDataSourceInstance() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addDataSet(
            this.dataSource2,
            DatabaseOperation.REFRESH,
            "schema",
            Resources.getResource(
                "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"));
    }

    /**
     * Tests that calling {@link DbUnitLoader#addProperty(String, String)} with a null key
     * correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The key cannot be null",
        groups = { "integration-tests" })
    public void testAddProperty_string_string_nullKey() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addProperty(
            null,
            "true");
    }

    /**
     * Tests that calling {@link DbUnitLoader#addProperty(String, String)} with a
     * multi-datasource instance correctly throws an {@link IllegalStateException}.
     */
    @Test(
        expectedExceptions = IllegalStateException.class,
        expectedExceptionsMessageRegExp = "Must specify a data source in multi-datasource instance",
        groups = { "integration-tests" })
    public static void testAddProperty_string_string_multiDataSourceInstance() {
        DbUnitLoader.createMultiDataSourceInstance().addProperty(
            DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES,
            "true");
    }

    /**
     * Tests that calling {@link DbUnitLoader#addProperty(DataSource, String, String)}
     * with a null {@link DataSource} correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The data source cannot be null",
        groups = { "integration-tests" })
    public static void testAddProperty_datasource_string_string_nullDataSource() {
        DbUnitLoader.createMultiDataSourceInstance().addProperty(
            null,
            DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES,
            "true");
    }

    /**
     * Tests that calling {@link DbUnitLoader#addProperty(DataSource, String, String)}
     * with a null key correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The key cannot be null",
        groups = { "integration-tests" })
    public void testAddProperty_datasource_string_string_nullKey() {
        DbUnitLoader.createMultiDataSourceInstance().addProperty(
            this.dataSource,
            null,
            "true");
    }

    /**
     * Tests that calling {@link DbUnitLoader#addProperty(DataSource, String, String)} on
     * a single-datasource instance with a second datasource passed correctly throws an
     * {@link IllegalStateException}.
     */
    @Test(
        expectedExceptions = IllegalStateException.class,
        expectedExceptionsMessageRegExp = "Cannot add second datasource to single-datasource instance",
        groups = { "integration-tests" })
    public void testAddProperty_datasource_string_string_singleDataSourceInstance() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).addProperty(
            this.dataSource2,
            DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES,
            "true");
    }

    /**
     * Tests that calling {@link DbUnitLoader#withProperties(Properties)} with null
     * properties correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The properties cannot be null",
        groups = { "integration-tests" })
    public void testWithProperties_properties_nullProperties() {
        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).withProperties(null);
    }

    /**
     * Tests that calling {@link DbUnitLoader#withProperties(Properties)} with a
     * multi-datasource instance correctly throws an {@link IllegalStateException}.
     */
    @Test(
        expectedExceptions = IllegalStateException.class,
        expectedExceptionsMessageRegExp = "Must specify a data source in multi-datasource instance",
        groups = { "integration-tests" })
    public static void testWithProperties_properties_multiDataSourceInstance() {
        Properties properties = new Properties();
        properties.setProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, "true");

        DbUnitLoader.createMultiDataSourceInstance().withProperties(properties);
    }

    /**
     * Tests that calling {@link DbUnitLoader#withProperties(DataSource, Properties)} with
     * a null {@link DataSource} correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The data source cannot be null",
        groups = { "integration-tests" })
    public static void testWithProperties_datasource_properties_nullDataSource() {
        Properties properties = new Properties();
        properties.setProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, "true");

        DbUnitLoader.createMultiDataSourceInstance().withProperties(null, properties);
    }

    /**
     * Tests that calling {@link DbUnitLoader#withProperties(DataSource, Properties)} with
     * null properties correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The properties cannot be null",
        groups = { "integration-tests" })
    public void testWithProperties_datasource_properties_nullProperties() {
        DbUnitLoader.createMultiDataSourceInstance().withProperties(
            this.dataSource,
            null);
    }

    /**
     * Tests that calling {@link DbUnitLoader#withProperties(DataSource, Properties)} on a
     * single-datasource instance with a second datasource passed correctly throws an
     * {@link IllegalStateException}.
     */
    @Test(
        expectedExceptions = IllegalStateException.class,
        expectedExceptionsMessageRegExp = "Cannot add second datasource to single-datasource instance",
        groups = { "integration-tests" })
    public void testWithProperties_datasource_properties_singleDataSource() {
        Properties properties = new Properties();
        properties.setProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, "true");

        DbUnitLoader.createSingleDataSourceInstance(this.dataSource).withProperties(
            this.dataSource2,
            properties);
    }

    /**
     * Tests that calling {@link DbUnitLoader#addReplacementData(String, Object)} with
     * null key correctly throws a {@link NullPointerException}.
     */
    @Test(
        expectedExceptions = NullPointerException.class,
        expectedExceptionsMessageRegExp = "The key cannot be null",
        groups = { "integration-tests" })
    public static void testAddReplacementData_nullKey() {
        DbUnitLoader.createMultiDataSourceInstance().addReplacementData(null, null);
    }

    /**
     * Tests that calling {@link DbUnitLoader#getReplacementData(String)} after inserting
     * the replacement data returns the correct result.
     */
    @Test(groups = { "integration-tests" })
    public static void testGetReplacementData() {
        Object data = "test string";
        DbUnitLoader loader = DbUnitLoader
                .createMultiDataSourceInstance()
                .addReplacementData("my-key", data);

        Assert.assertEquals(
            loader.getReplacementData("my-key"),
            data,
            "Incorrect replacement data returned");
    }

    /**
     * Tests that calling {@link DbUnitLoader#getReplacementData(String)} after inserting
     * {@code null} replacement data returns the correct result.
     */
    @Test(groups = { "integration-tests" })
    public static void testGetReplacementData_nullData() {
        DbUnitLoader loader = DbUnitLoader
                .createMultiDataSourceInstance()
                .addReplacementData("my-key", null);

        Assert.assertNull(
            loader.getReplacementData("my-key"),
            "Incorrect replacement data returned");
    }

    /**
     * Tests that calling {@link DbUnitLoader#getReplacementData(String)} after not
     * inserting replacement data returns {@code null}.
     */
    @Test(groups = { "integration-tests" })
    public static void testGetReplacementData_noData() {
        DbUnitLoader loader = DbUnitLoader.createMultiDataSourceInstance();

        Assert.assertNull(
            loader.getReplacementData("my-key"),
            "Incorrect replacement data returned");
    }

    /**
     * Tests that loading the database using a single-datasource instance with data by
     * calling {@link DbUnitLoader#addDataSet(String, String)} correctly loads the
     * database.
     */
    @Test(groups = { "integration-tests" })
    public void testLoad_single_addDataSource_string_string() {
        DbUnitLoader
                .createSingleDataSourceInstance(this.dataSource)
                .addDataSet(
                    "test",
                    "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml")
                .addProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, "false")
                .addProperty(DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES, "true")
                .addProperty(
                    DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                    "org.dbunit.ext.h2.H2DataTypeFactory")
                .load();

        String fullQuery = "SELECT * FROM test.tst1_test1_tab;";
        List<Map<String, Object>> fullTableList = this.jdbcTemplate.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList.size(),
            2,
            "Incorrect number of records in table");

        String record1Query = "SELECT * FROM test.tst1_test1_tab WHERE tst1_keyint= -1";
        Map<String, Object> record1 = this.jdbcTemplate.queryForMap(record1Query);
        LocalDateTime dateTime = LocalDateTime.of(
            2012,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);
        Assert.assertEquals(
            record1.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record1.get("tst1_colvarchar"),
            "test 1",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record1.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record1.get("tst1_lastupdateid"),
            "test_user",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record1.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime),
            "Incorrect lastupdatedate value");
    }

    /**
     * Tests that loading the database using a single-datasource instance with data by
     * calling {@link DbUnitLoader#addDataSet(String, String, Class)} correctly loads the
     * database.
     */
    @Test(groups = { "integration-tests" })
    public void testLoad_single_addDataSource_string_string_class() {
        DbUnitLoader
                .createSingleDataSourceInstance(this.dataSource)
                .addDataSet(
                    "test",
                    "dataset1_without_replacements.xml",
                    DbUnitLoaderTest.class)
                .addProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, "false")
                .addProperty(DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES, "true")
                .addProperty(
                    DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                    "org.dbunit.ext.h2.H2DataTypeFactory")
                .load();

        String fullQuery = "SELECT * FROM test.tst1_test1_tab;";
        List<Map<String, Object>> fullTableList = this.jdbcTemplate.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList.size(),
            2,
            "Incorrect number of records in table");

        String record1Query = "SELECT * FROM test.tst1_test1_tab WHERE tst1_keyint= -1";
        Map<String, Object> record1 = this.jdbcTemplate.queryForMap(record1Query);
        LocalDateTime dateTime = LocalDateTime.of(
            2012,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);
        Assert.assertEquals(
            record1.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record1.get("tst1_colvarchar"),
            "test 1",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record1.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record1.get("tst1_lastupdateid"),
            "test_user",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record1.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime),
            "Incorrect lastupdatedate value");
    }

    /**
     * Tests that loading the database using a single-datasource instance with data by
     * calling {@link DbUnitLoader#addDataSet(String, URL)} correctly loads the database.
     */
    @Test(groups = { "integration-tests" })
    public void testLoad_single_addDataSource_string_url() {
        DbUnitLoader
                .createSingleDataSourceInstance(this.dataSource)
                .addDataSet(
                    "test",
                    Resources.getResource(
                        "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"))
                .addProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, "false")
                .addProperty(DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES, "true")
                .addProperty(
                    DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                    "org.dbunit.ext.h2.H2DataTypeFactory")
                .load();

        String fullQuery = "SELECT * FROM test.tst1_test1_tab;";
        List<Map<String, Object>> fullTableList = this.jdbcTemplate.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList.size(),
            2,
            "Incorrect number of records in table");

        String record1Query = "SELECT * FROM test.tst1_test1_tab WHERE tst1_keyint= -1";
        Map<String, Object> record1 = this.jdbcTemplate.queryForMap(record1Query);
        LocalDateTime dateTime = LocalDateTime.of(
            2012,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);
        Assert.assertEquals(
            record1.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record1.get("tst1_colvarchar"),
            "test 1",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record1.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record1.get("tst1_lastupdateid"),
            "test_user",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record1.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime),
            "Incorrect lastupdatedate value");
    }

    /**
     * Tests that loading the database using a single-datasource instance with data by
     * calling {@link DbUnitLoader#addDataSet(DatabaseOperation, String, String)}
     * correctly loads the database.
     */
    @Test(groups = { "integration-tests" })
    public void testLoad_single_addDataSource_databaseoperation_string_string() {
        DbUnitLoader
                .createSingleDataSourceInstance(this.dataSource)
                .addDataSet(
                    DatabaseOperation.REFRESH,
                    "test",
                    "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml")
                .addProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, "false")
                .addProperty(DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES, "true")
                .addProperty(
                    DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                    "org.dbunit.ext.h2.H2DataTypeFactory")
                .load();

        String fullQuery = "SELECT * FROM test.tst1_test1_tab;";
        List<Map<String, Object>> fullTableList = this.jdbcTemplate.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList.size(),
            2,
            "Incorrect number of records in table");

        String record1Query = "SELECT * FROM test.tst1_test1_tab WHERE tst1_keyint= -1";
        Map<String, Object> record1 = this.jdbcTemplate.queryForMap(record1Query);
        LocalDateTime dateTime = LocalDateTime.of(
            2012,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);
        Assert.assertEquals(
            record1.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record1.get("tst1_colvarchar"),
            "test 1",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record1.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record1.get("tst1_lastupdateid"),
            "test_user",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record1.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime),
            "Incorrect lastupdatedate value");
    }

    /**
     * Tests that loading the database using a single-datasource instance with data by
     * calling {@link DbUnitLoader#addDataSet(DatabaseOperation, String, String, Class)}
     * correctly loads the database.
     */
    @Test(groups = { "integration-tests" })
    public void testLoad_single_addDataSource_databaseoperation_string_string_class() {
        DbUnitLoader
                .createSingleDataSourceInstance(this.dataSource)
                .addDataSet(
                    DatabaseOperation.REFRESH,
                    "test",
                    "dataset1_without_replacements.xml",
                    DbUnitLoaderTest.class)
                .addProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, "false")
                .addProperty(DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES, "true")
                .addProperty(
                    DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                    "org.dbunit.ext.h2.H2DataTypeFactory")
                .load();

        String fullQuery = "SELECT * FROM test.tst1_test1_tab;";
        List<Map<String, Object>> fullTableList = this.jdbcTemplate.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList.size(),
            2,
            "Incorrect number of records in table");

        String record1Query = "SELECT * FROM test.tst1_test1_tab WHERE tst1_keyint= -1";
        Map<String, Object> record1 = this.jdbcTemplate.queryForMap(record1Query);
        LocalDateTime dateTime = LocalDateTime.of(
            2012,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);
        Assert.assertEquals(
            record1.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record1.get("tst1_colvarchar"),
            "test 1",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record1.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record1.get("tst1_lastupdateid"),
            "test_user",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record1.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime),
            "Incorrect lastupdatedate value");
    }

    /**
     * Tests that loading the database using a single-datasource instance with data by
     * calling {@link DbUnitLoader#addDataSet(DatabaseOperation, String, URL)} correctly
     * loads the database.
     */
    @Test(groups = { "integration-tests" })
    public void testLoad_single_addDataSource_databaseoperation_string_url() {
        DbUnitLoader
                .createSingleDataSourceInstance(this.dataSource)
                .addDataSet(
                    DatabaseOperation.REFRESH,
                    "test",
                    Resources.getResource(
                        "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"))
                .addProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, "false")
                .addProperty(DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES, "true")
                .addProperty(
                    DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                    "org.dbunit.ext.h2.H2DataTypeFactory")
                .load();

        String fullQuery = "SELECT * FROM test.tst1_test1_tab;";
        List<Map<String, Object>> fullTableList = this.jdbcTemplate.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList.size(),
            2,
            "Incorrect number of records in table");

        String record1Query = "SELECT * FROM test.tst1_test1_tab WHERE tst1_keyint= -1";
        Map<String, Object> record1 = this.jdbcTemplate.queryForMap(record1Query);
        LocalDateTime dateTime = LocalDateTime.of(
            2012,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);
        Assert.assertEquals(
            record1.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record1.get("tst1_colvarchar"),
            "test 1",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record1.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record1.get("tst1_lastupdateid"),
            "test_user",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record1.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime),
            "Incorrect lastupdatedate value");
    }

    /**
     * Tests that loading the database using a multi-datasource instance with data by
     * calling {@link DbUnitLoader#addDataSet(DataSource, String, String)} correctly loads
     * the databases.
     */
    @Test(groups = { "integration-tests" })
    public void testLoad_multi_addDataSource_string_string() {
        DbUnitLoader
                .createMultiDataSourceInstance()
                .addDataSet(
                    this.dataSource,
                    "test",
                    "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml")
                .addDataSet(
                    this.dataSource2,
                    "test",
                    "ca/ammeter/test/utilities/dbunit/dataset2_without_replacements.xml")
                .addProperty(
                    this.dataSource,
                    DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES,
                    "false")
                .addProperty(
                    this.dataSource2,
                    DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES,
                    "false")
                .addProperty(
                    this.dataSource,
                    DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES,
                    "true")
                .addProperty(
                    this.dataSource2,
                    DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES,
                    "true")
                .addProperty(
                    this.dataSource,
                    DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                    "org.dbunit.ext.h2.H2DataTypeFactory")
                .addProperty(
                    this.dataSource2,
                    DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                    "org.dbunit.ext.h2.H2DataTypeFactory")
                .load();

        String fullQuery = "SELECT * FROM test.tst1_test1_tab;";

        List<Map<String, Object>> fullTableList = this.jdbcTemplate.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList.size(),
            2,
            "Incorrect number of records in table");

        List<Map<String, Object>> fullTableList2 = this.jdbcTemplate2.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList2.size(),
            1,
            "Incorrect number of records in table");

        String record1Query = "SELECT * FROM test.tst1_test1_tab WHERE tst1_keyint= -1";

        LocalDateTime dateTime1 = LocalDateTime.of(
            2012,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);
        Map<String, Object> record1 = this.jdbcTemplate.queryForMap(record1Query);
        Assert.assertEquals(
            record1.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record1.get("tst1_colvarchar"),
            "test 1",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record1.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime1),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record1.get("tst1_lastupdateid"),
            "test_user",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record1.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime1),
            "Incorrect lastupdatedate value");

        LocalDateTime dateTime2 = LocalDateTime.of(
            2013,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);
        Map<String, Object> record2 = this.jdbcTemplate2.queryForMap(record1Query);
        Assert.assertEquals(
            record2.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record2.get("tst1_colvarchar"),
            "test 2-1",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record2.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime2),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record2.get("tst1_lastupdateid"),
            "test_user2",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record2.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime2),
            "Incorrect lastupdatedate value");
    }

    /**
     * Tests that loading the database using a multi-datasource instance with data by
     * calling {@link DbUnitLoader#addDataSet(DataSource, String, String, Class)}
     * correctly loads the databases.
     */
    @Test(groups = { "integration-tests" })
    public void testLoad_multi_addDataSource_string_string_class() {
        DbUnitLoader
                .createMultiDataSourceInstance()
                .addDataSet(
                    this.dataSource,
                    "test",
                    "dataset1_without_replacements.xml",
                    DbUnitLoaderTest.class)
                .addDataSet(
                    this.dataSource2,
                    "test",
                    "dataset2_without_replacements.xml",
                    DbUnitLoaderTest.class)
                .addProperty(
                    this.dataSource,
                    DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES,
                    "false")
                .addProperty(
                    this.dataSource2,
                    DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES,
                    "false")
                .addProperty(
                    this.dataSource,
                    DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES,
                    "true")
                .addProperty(
                    this.dataSource2,
                    DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES,
                    "true")
                .addProperty(
                    this.dataSource,
                    DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                    "org.dbunit.ext.h2.H2DataTypeFactory")
                .addProperty(
                    this.dataSource2,
                    DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                    "org.dbunit.ext.h2.H2DataTypeFactory")
                .load();

        String fullQuery = "SELECT * FROM test.tst1_test1_tab;";

        List<Map<String, Object>> fullTableList = this.jdbcTemplate.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList.size(),
            2,
            "Incorrect number of records in table");

        List<Map<String, Object>> fullTableList2 = this.jdbcTemplate2.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList2.size(),
            1,
            "Incorrect number of records in table");

        String record1Query = "SELECT * FROM test.tst1_test1_tab WHERE tst1_keyint= -1";

        LocalDateTime dateTime1 = LocalDateTime.of(
            2012,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);
        Map<String, Object> record1 = this.jdbcTemplate.queryForMap(record1Query);
        Assert.assertEquals(
            record1.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record1.get("tst1_colvarchar"),
            "test 1",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record1.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime1),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record1.get("tst1_lastupdateid"),
            "test_user",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record1.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime1),
            "Incorrect lastupdatedate value");

        LocalDateTime dateTime2 = LocalDateTime.of(
            2013,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);
        Map<String, Object> record2 = this.jdbcTemplate2.queryForMap(record1Query);
        Assert.assertEquals(
            record2.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record2.get("tst1_colvarchar"),
            "test 2-1",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record2.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime2),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record2.get("tst1_lastupdateid"),
            "test_user2",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record2.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime2),
            "Incorrect lastupdatedate value");
    }

    /**
     * Tests that loading the database using a multi-datasource instance with data by
     * calling {@link DbUnitLoader#addDataSet(DataSource, String, URL)} correctly loads
     * the databases.
     */
    @Test(groups = { "integration-tests" })
    public void testLoad_multi_addDataSource_string_url() {
        DbUnitLoader
                .createMultiDataSourceInstance()
                .addDataSet(
                    this.dataSource,
                    "test",
                    Resources.getResource(
                        "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"))
                .addDataSet(
                    this.dataSource2,
                    "test",
                    Resources.getResource(
                        "ca/ammeter/test/utilities/dbunit/dataset2_without_replacements.xml"))
                .addProperty(
                    this.dataSource,
                    DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES,
                    "false")
                .addProperty(
                    this.dataSource2,
                    DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES,
                    "false")
                .addProperty(
                    this.dataSource,
                    DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES,
                    "true")
                .addProperty(
                    this.dataSource2,
                    DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES,
                    "true")
                .addProperty(
                    this.dataSource,
                    DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                    "org.dbunit.ext.h2.H2DataTypeFactory")
                .addProperty(
                    this.dataSource2,
                    DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                    "org.dbunit.ext.h2.H2DataTypeFactory")
                .load();

        String fullQuery = "SELECT * FROM test.tst1_test1_tab;";

        List<Map<String, Object>> fullTableList = this.jdbcTemplate.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList.size(),
            2,
            "Incorrect number of records in table");

        List<Map<String, Object>> fullTableList2 = this.jdbcTemplate2.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList2.size(),
            1,
            "Incorrect number of records in table");

        String record1Query = "SELECT * FROM test.tst1_test1_tab WHERE tst1_keyint= -1";
        LocalDateTime dateTime1 = LocalDateTime.of(
            2012,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);
        Map<String, Object> record1 = this.jdbcTemplate.queryForMap(record1Query);
        Assert.assertEquals(
            record1.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record1.get("tst1_colvarchar"),
            "test 1",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record1.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime1),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record1.get("tst1_lastupdateid"),
            "test_user",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record1.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime1),
            "Incorrect lastupdatedate value");

        LocalDateTime dateTime2 = LocalDateTime.of(
            2013,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);
        Map<String, Object> record2 = this.jdbcTemplate2.queryForMap(record1Query);
        Assert.assertEquals(
            record2.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record2.get("tst1_colvarchar"),
            "test 2-1",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record2.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime2),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record2.get("tst1_lastupdateid"),
            "test_user2",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record2.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime2),
            "Incorrect lastupdatedate value");
    }

    /**
     * Tests that loading the database using a multi-datasource instance with data by
     * calling
     * {@link DbUnitLoader#addDataSet(DataSource, DatabaseOperation, String, String)}
     * correctly loads the databases.
     */
    @Test(groups = { "integration-tests" })
    public void testLoad_multi_addDataSource_databaseoperation_string_string() {
        DbUnitLoader
                .createMultiDataSourceInstance()
                .addDataSet(
                    this.dataSource,
                    DatabaseOperation.REFRESH,
                    "test",
                    "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml")
                .addDataSet(
                    this.dataSource2,
                    DatabaseOperation.REFRESH,
                    "test",
                    "ca/ammeter/test/utilities/dbunit/dataset2_without_replacements.xml")
                .addProperty(
                    this.dataSource,
                    DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES,
                    "false")
                .addProperty(
                    this.dataSource2,
                    DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES,
                    "false")
                .addProperty(
                    this.dataSource,
                    DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES,
                    "true")
                .addProperty(
                    this.dataSource2,
                    DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES,
                    "true")
                .addProperty(
                    this.dataSource,
                    DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                    "org.dbunit.ext.h2.H2DataTypeFactory")
                .addProperty(
                    this.dataSource2,
                    DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                    "org.dbunit.ext.h2.H2DataTypeFactory")
                .load();

        String fullQuery = "SELECT * FROM test.tst1_test1_tab;";

        List<Map<String, Object>> fullTableList = this.jdbcTemplate.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList.size(),
            2,
            "Incorrect number of records in table");

        List<Map<String, Object>> fullTableList2 = this.jdbcTemplate2.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList2.size(),
            1,
            "Incorrect number of records in table");

        String record1Query = "SELECT * FROM test.tst1_test1_tab WHERE tst1_keyint= -1";

        LocalDateTime dateTime1 = LocalDateTime.of(
            2012,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);
        Map<String, Object> record1 = this.jdbcTemplate.queryForMap(record1Query);
        Assert.assertEquals(
            record1.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record1.get("tst1_colvarchar"),
            "test 1",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record1.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime1),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record1.get("tst1_lastupdateid"),
            "test_user",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record1.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime1),
            "Incorrect lastupdatedate value");

        LocalDateTime dateTime2 = LocalDateTime.of(
            2013,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);
        Map<String, Object> record2 = this.jdbcTemplate2.queryForMap(record1Query);
        Assert.assertEquals(
            record2.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record2.get("tst1_colvarchar"),
            "test 2-1",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record2.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime2),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record2.get("tst1_lastupdateid"),
            "test_user2",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record2.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime2),
            "Incorrect lastupdatedate value");
    }

    /**
     * Tests that loading the database using a multi-datasource instance with data by
     * calling
     * {@link DbUnitLoader#addDataSet(DataSource, DatabaseOperation, String, String, Class)}
     * correctly loads the databases.
     */
    @Test(groups = { "integration-tests" })
    public void testLoad_multi_addDataSource_databaseoperation_string_string_class() {
        DbUnitLoader
                .createMultiDataSourceInstance()
                .addDataSet(
                    this.dataSource,
                    DatabaseOperation.REFRESH,
                    "test",
                    "dataset1_without_replacements.xml",
                    DbUnitLoaderTest.class)
                .addDataSet(
                    this.dataSource2,
                    DatabaseOperation.REFRESH,
                    "test",
                    "dataset2_without_replacements.xml",
                    DbUnitLoaderTest.class)
                .addProperty(
                    this.dataSource,
                    DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES,
                    "false")
                .addProperty(
                    this.dataSource2,
                    DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES,
                    "false")
                .addProperty(
                    this.dataSource,
                    DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES,
                    "true")
                .addProperty(
                    this.dataSource2,
                    DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES,
                    "true")
                .addProperty(
                    this.dataSource,
                    DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                    "org.dbunit.ext.h2.H2DataTypeFactory")
                .addProperty(
                    this.dataSource2,
                    DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                    "org.dbunit.ext.h2.H2DataTypeFactory")
                .load();

        String fullQuery = "SELECT * FROM test.tst1_test1_tab;";

        List<Map<String, Object>> fullTableList = this.jdbcTemplate.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList.size(),
            2,
            "Incorrect number of records in table");

        List<Map<String, Object>> fullTableList2 = this.jdbcTemplate2.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList2.size(),
            1,
            "Incorrect number of records in table");

        String record1Query = "SELECT * FROM test.tst1_test1_tab WHERE tst1_keyint= -1";

        LocalDateTime dateTime1 = LocalDateTime.of(
            2012,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);
        Map<String, Object> record1 = this.jdbcTemplate.queryForMap(record1Query);
        Assert.assertEquals(
            record1.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record1.get("tst1_colvarchar"),
            "test 1",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record1.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime1),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record1.get("tst1_lastupdateid"),
            "test_user",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record1.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime1),
            "Incorrect lastupdatedate value");

        LocalDateTime dateTime2 = LocalDateTime.of(
            2013,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);
        Map<String, Object> record2 = this.jdbcTemplate2.queryForMap(record1Query);
        Assert.assertEquals(
            record2.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record2.get("tst1_colvarchar"),
            "test 2-1",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record2.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime2),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record2.get("tst1_lastupdateid"),
            "test_user2",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record2.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime2),
            "Incorrect lastupdatedate value");
    }

    /**
     * Tests that loading the database using a multi-datasource instance with data by
     * calling {@link DbUnitLoader#addDataSet(DataSource, DatabaseOperation, String, URL)}
     * correctly loads the databases.
     */
    @Test(groups = { "integration-tests" })
    public void testLoad_multi_addDataSource_databaseoperation_string_url() {
        DbUnitLoader
                .createMultiDataSourceInstance()
                .addDataSet(
                    this.dataSource,
                    DatabaseOperation.REFRESH,
                    "test",
                    Resources.getResource(
                        "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml"))
                .addDataSet(
                    this.dataSource2,
                    DatabaseOperation.REFRESH,
                    "test",
                    Resources.getResource(
                        "ca/ammeter/test/utilities/dbunit/dataset2_without_replacements.xml"))
                .addProperty(
                    this.dataSource,
                    DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES,
                    "false")
                .addProperty(
                    this.dataSource2,
                    DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES,
                    "false")
                .addProperty(
                    this.dataSource,
                    DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES,
                    "true")
                .addProperty(
                    this.dataSource2,
                    DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES,
                    "true")
                .addProperty(
                    this.dataSource,
                    DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                    "org.dbunit.ext.h2.H2DataTypeFactory")
                .addProperty(
                    this.dataSource2,
                    DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                    "org.dbunit.ext.h2.H2DataTypeFactory")
                .load();

        String fullQuery = "SELECT * FROM test.tst1_test1_tab;";

        List<Map<String, Object>> fullTableList = this.jdbcTemplate.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList.size(),
            2,
            "Incorrect number of records in table");

        List<Map<String, Object>> fullTableList2 = this.jdbcTemplate2.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList2.size(),
            1,
            "Incorrect number of records in table");

        String record1Query = "SELECT * FROM test.tst1_test1_tab WHERE tst1_keyint= -1";
        LocalDateTime dateTime1 = LocalDateTime.of(
            2012,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);
        Map<String, Object> record1 = this.jdbcTemplate.queryForMap(record1Query);
        Assert.assertEquals(
            record1.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record1.get("tst1_colvarchar"),
            "test 1",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record1.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime1),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record1.get("tst1_lastupdateid"),
            "test_user",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record1.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime1),
            "Incorrect lastupdatedate value");

        LocalDateTime dateTime2 = LocalDateTime.of(
            2013,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);
        Map<String, Object> record2 = this.jdbcTemplate2.queryForMap(record1Query);
        Assert.assertEquals(
            record2.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record2.get("tst1_colvarchar"),
            "test 2-1",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record2.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime2),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record2.get("tst1_lastupdateid"),
            "test_user2",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record2.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime2),
            "Incorrect lastupdatedate value");
    }

    /**
     * Tests that loading the database using a single-datasource instance with properties
     * by calling {@link DbUnitLoader#withProperties(Properties)} correctly loads the
     * database.
     */
    @Test(groups = { "integration-tests" })
    public void testLoad_single_withProperties() {
        Properties properties = new Properties();
        properties.setProperty(
            DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES,
            "false");
        properties.setProperty(DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES, "true");
        properties.setProperty(
            DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
            "org.dbunit.ext.h2.H2DataTypeFactory");

        DbUnitLoader
                .createSingleDataSourceInstance(this.dataSource)
                .addDataSet(
                    "test",
                    "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml")
                .withProperties(properties)
                .load();

        String fullQuery = "SELECT * FROM test.tst1_test1_tab;";
        List<Map<String, Object>> fullTableList = this.jdbcTemplate.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList.size(),
            2,
            "Incorrect number of records in table");

        String record1Query = "SELECT * FROM test.tst1_test1_tab WHERE tst1_keyint= -1";
        Map<String, Object> record1 = this.jdbcTemplate.queryForMap(record1Query);
        LocalDateTime dateTime = LocalDateTime.of(
            2012,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);
        Assert.assertEquals(
            record1.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record1.get("tst1_colvarchar"),
            "test 1",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record1.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record1.get("tst1_lastupdateid"),
            "test_user",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record1.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime),
            "Incorrect lastupdatedate value");
    }

    /**
     * Tests that loading the database using a multi-datasource instance with properties
     * by calling {@link DbUnitLoader#withProperties(DataSource, Properties)} correctly
     * loads the databases.
     */
    @Test(groups = { "integration-tests" })
    public void testLoad_multi_withProperties() {
        Properties properties = new Properties();
        properties.setProperty(
            DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES,
            "false");
        properties.setProperty(DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES, "true");
        properties.setProperty(
            DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
            "org.dbunit.ext.h2.H2DataTypeFactory");

        DbUnitLoader
                .createMultiDataSourceInstance()
                .addDataSet(
                    this.dataSource,
                    "test",
                    "ca/ammeter/test/utilities/dbunit/dataset1_without_replacements.xml")
                .addDataSet(
                    this.dataSource2,
                    "test",
                    "ca/ammeter/test/utilities/dbunit/dataset2_without_replacements.xml")
                .withProperties(this.dataSource, properties)
                .withProperties(this.dataSource2, properties)
                .load();

        String fullQuery = "SELECT * FROM test.tst1_test1_tab;";

        List<Map<String, Object>> fullTableList = this.jdbcTemplate.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList.size(),
            2,
            "Incorrect number of records in table");

        List<Map<String, Object>> fullTableList2 = this.jdbcTemplate2.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList2.size(),
            1,
            "Incorrect number of records in table");

        String record1Query = "SELECT * FROM test.tst1_test1_tab WHERE tst1_keyint= -1";
        LocalDateTime dateTime1 = LocalDateTime.of(
            2012,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);
        Map<String, Object> record1 = this.jdbcTemplate.queryForMap(record1Query);
        Assert.assertEquals(
            record1.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record1.get("tst1_colvarchar"),
            "test 1",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record1.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime1),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record1.get("tst1_lastupdateid"),
            "test_user",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record1.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime1),
            "Incorrect lastupdatedate value");

        LocalDateTime dateTime2 = LocalDateTime.of(
            2013,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);
        Map<String, Object> record2 = this.jdbcTemplate2.queryForMap(record1Query);
        Assert.assertEquals(
            record2.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record2.get("tst1_colvarchar"),
            "test 2-1",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record2.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime2),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record2.get("tst1_lastupdateid"),
            "test_user2",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record2.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime2),
            "Incorrect lastupdatedate value");
    }

    /**
     * Tests that loading the database using a multi-datasource instance while using
     * replacement data correctly loads the databases.
     */
    @Test(groups = { "integration-tests" })
    public void testLoad_addReplacementData_withDefaultReplacementData() {
        LocalDateTime dateTime = LocalDateTime.of(
            2012,
            Month.MARCH,
            13,
            21,
            52,
            3,
            587000000);

        List<Map<String, Object>> firstQuery = this.jdbcTemplate.queryForList(
            "SELECT VALUE FROM INFORMATION_SCHEMA.SETTINGS WHERE NAME='MODE';");
        System.out.println(firstQuery);
        List<Map<String, Object>> secondQuery = this.jdbcTemplate.queryForList(
            "SELECT SCHEMA_NAME TABLE_SCHEM, CATALOG_NAME TABLE_CATALOG, IS_DEFAULT FROM INFORMATION_SCHEMA.SCHEMATA ORDER BY SCHEMA_NAME;");
        System.out.println(secondQuery);
        List<Map<String, Object>> thirdQuery = this.jdbcTemplate.queryForList(
            "SELECT TABLE_CATALOG TABLE_CAT, TABLE_SCHEMA TABLE_SCHEM, TABLE_NAME, TABLE_TYPE, REMARKS, TYPE_NAME TYPE_CAT, TYPE_NAME TYPE_SCHEM, TYPE_NAME, TYPE_NAME SELF_REFERENCING_COL_NAME, TYPE_NAME REF_GENERATION, SQL FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_CATALOG LIKE '%' ESCAPE '\\' AND TABLE_SCHEMA LIKE 'test' ESCAPE '\\' AND TABLE_NAME LIKE '%' ESCAPE '\\' AND (TABLE_TYPE IN('TABLE')) ORDER BY TABLE_TYPE, TABLE_SCHEMA, TABLE_NAME;");
        System.out.println(thirdQuery);

        DbUnitLoader
                .createMultiDataSourceInstance()
                .addDataSet(
                    this.dataSource,
                    "test",
                    "ca/ammeter/test/utilities/dbunit/dataset1_with_replacements.xml")
                .addDataSet(
                    this.dataSource2,
                    "test",
                    "ca/ammeter/test/utilities/dbunit/dataset2_with_replacements.xml")
                .addProperty(
                    this.dataSource,
                    DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES,
                    "false")
                .addProperty(
                    this.dataSource2,
                    DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES,
                    "false")
                .addProperty(
                    this.dataSource,
                    DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES,
                    "true")
                .addProperty(
                    this.dataSource2,
                    DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES,
                    "true")
                .addProperty(
                    this.dataSource,
                    DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                    "org.dbunit.ext.h2.H2DataTypeFactory")
                .addProperty(
                    this.dataSource2,
                    DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                    "org.dbunit.ext.h2.H2DataTypeFactory")
                .withDefaultReplacementData()
                .addReplacementData("[REPLACE]", "some replacement data")
                .addReplacementData("[REPLACE2]", "some replacement data - 2")
                .addReplacementData("[SYSDATE]", Timestamp.valueOf(dateTime))
                .load();

        String fullQuery = "SELECT * FROM test.tst1_test1_tab;";

        List<Map<String, Object>> fullTableList = this.jdbcTemplate.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList.size(),
            2,
            "Incorrect number of records in table");

        List<Map<String, Object>> fullTableList2 = this.jdbcTemplate2.queryForList(
            fullQuery);
        Assert.assertEquals(
            fullTableList2.size(),
            1,
            "Incorrect number of records in table");

        String record1Query = "SELECT * FROM test.tst1_test1_tab WHERE tst1_keyint= -1";
        Map<String, Object> record1 = this.jdbcTemplate.queryForMap(record1Query);
        Assert.assertEquals(
            record1.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record1.get("tst1_colvarchar"),
            "some replacement data",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record1.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record1.get("tst1_lastupdateid"),
            "test_user",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record1.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime),
            "Incorrect lastupdatedate value");

        Map<String, Object> record2 = this.jdbcTemplate2.queryForMap(record1Query);
        Assert.assertEquals(
            record2.get("tst1_keyint"),
            BigDecimal.valueOf(-1),
            "Incorrect keyint value");
        Assert.assertEquals(
            record2.get("tst1_colvarchar"),
            "some replacement data - 2",
            "Incorrect colvarchar value");
        Assert.assertEquals(
            record2.get("tst1_coldatetime"),
            Timestamp.valueOf(dateTime),
            "Incorrect coldatetime value");
        Assert.assertEquals(
            record2.get("tst1_lastupdateid"),
            "test_user",
            "Incorrect lastupdateid value");
        Assert.assertEquals(
            record2.get("tst1_lastupdatedate"),
            Timestamp.valueOf(dateTime),
            "Incorrect lastupdatedate value");
    }

    /**
     * Verifies that the {@link DbUnitLoader} utility correctly rolls back inserts.
     */
    @AfterClass
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void testDatabaseEmptyAfterTests_TEST_11() {
        String fullQuery = "SELECT * FROM test.tst1_test1_tab;";

        List<Map<String, Object>> fullTableList = this.jdbcTemplate.queryForList(
            fullQuery);
        List<Map<String, Object>> fullTableList2 = this.jdbcTemplate2.queryForList(
            fullQuery);

        Assert.assertEquals(
            fullTableList.size(),
            0,
            "Incorrect number of records in table from database 1");
        Assert.assertEquals(
            fullTableList2.size(),
            0,
            "Incorrect number of records in table from database 2");
    }
}
