/*
 * Copyright 2016 Matthew Ammeter
 *
 * This file is part of test-utilities.
 *
 * test-utilities is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * test-utilities is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with test-utilities.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.ammeter.test.utilities.asserts;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.testng.annotations.Test;

/**
 * Tests for {@link DateTimeAssert}.
 *
 * @author Matthew Ammeter
 */
public class DateTimeAssertTest {

    /**
     * Tests that passing a {@code null} {@code expected} {@link Calendar} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[null\\] but found \\[Fri May 01 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_calendar_nullExpected() {
        Calendar actual = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertAfter(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link Calendar} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[Fri May 01 15:15:15 CDT 2015\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_calendar_nullActual() {
        Calendar expected = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertAfter(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link Calendar} and
     * {@code null} {@code actual} {@link Calendar} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_calendar_nullExpectedAndActual() {
        DateTimeAssert.assertAfter((Calendar) null, (Calendar) null, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Calendar} that is before the
     * {@code expected} {@link Calendar} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[Sat May 02 15:15:15 CDT 2015\\] but found \\[Fri May 01 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_calendar_testFail() {
        Calendar actual = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        Calendar expected = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Calendar} that is before the
     * {@code expected} {@link Calendar} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected >\\[Sat May 02 15:15:15 CDT 2015\\] but found \\[Fri May 01 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_calendar_testFailNullMessage() {
        Calendar actual = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        Calendar expected = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertAfter(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link Calendar} that is equal to the
     * {@code expected} {@link Calendar} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[Fri May 01 15:15:15 CDT 2015\\] but found \\[Fri May 01 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_calendar_testFailEqual() {
        Calendar actual = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertAfter(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Calendar} that is after the
     * {@code expected} {@link Calendar} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertAfter_calendar_testPass() {
        Calendar actual = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        Calendar expected = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link Date} correctly throws an
     * {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[null\\] but found \\[Fri May 01 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_date_nullExpected() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertAfter(Date.from(actual), null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link Date} correctly throws an
     * {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[Fri May 01 15:15:15 CDT 2015\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_date_nullActual() {
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertAfter(null, Date.from(expected), "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link Date} and
     * {@code null} {@code actual} {@link Date} correctly throws an {@link AssertionError}
     * .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_date_nullExpectedAndActual() {
        DateTimeAssert.assertAfter((Date) null, (Date) null, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Date} that is before the
     * {@code expected} {@link Date} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[Sat May 02 15:15:15 CDT 2015\\] but found \\[Fri May 01 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_date_testFail() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertAfter(
            Date.from(actual),
            Date.from(expected),
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Date} that is before the
     * {@code expected} {@link Date} with a {@code null} {@code message} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected >\\[Sat May 02 15:15:15 CDT 2015\\] but found \\[Fri May 01 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_date_testFailNullMessage() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertAfter(Date.from(actual), Date.from(expected), null);
    }

    /**
     * Tests that passing an {@code actual} {@link Date} that is equal to the
     * {@code expected} {@link Date} with a {@code null} {@code message} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[Fri May 01 15:15:15 CDT 2015\\] but found \\[Fri May 01 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_date_testFailEqual() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertAfter(Date.from(actual), Date.from(actual), "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Date} that is after the
     * {@code expected} {@link Date} correctly does not throw an {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertAfter_date_testPass() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertAfter(
            Date.from(actual),
            Date.from(expected),
            "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link Instant} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[null\\] but found \\[2015-05-01T20:15:15Z\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_instant_nullExpected() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertAfter(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link Instant} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[2015-05-01T20:15:15Z\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_instant_nullActual() {
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertAfter(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link Instant} and
     * {@code null} {@code actual} {@link Instant} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_instant_nullExpectedAndActual() {
        DateTimeAssert.assertAfter((Instant) null, (Instant) null, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Instant} that is before the
     * {@code expected} {@link Instant} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[2015-05-02T20:15:15Z\\] but found \\[2015-05-01T20:15:15Z\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_instant_testFail() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Instant} that is before the
     * {@code expected} {@link Instant} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected >\\[2015-05-02T20:15:15Z\\] but found \\[2015-05-01T20:15:15Z\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_instant_testFailNullMessage() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertAfter(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link Instant} that is equal to the
     * {@code expected} {@link Instant} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[2015-05-01T20:15:15Z\\] but found \\[2015-05-01T20:15:15Z\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_instant_testFailEqual() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertAfter(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Instant} that is after the
     * {@code expected} {@link Instant} correctly does not throw an {@link AssertionError}
     * .
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertAfter_instant_testPass() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link LocalDate} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[null\\] but found \\[2015-05-01\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_localDate_nullExpected() {
        LocalDate actual = LocalDate.of(2015, Month.MAY, 1);
        DateTimeAssert.assertAfter(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link LocalDate} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[2015-05-01\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_localDate_nullActual() {
        LocalDate expected = LocalDate.of(2015, Month.MAY, 1);
        DateTimeAssert.assertAfter(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link LocalDate} and
     * {@code null} {@code actual} {@link LocalDate} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_localDate_nullExpectedAndActual() {
        DateTimeAssert.assertAfter((LocalDate) null, (LocalDate) null, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDate} that is before the
     * {@code expected} {@link LocalDate} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[2015-05-02\\] but found \\[2015-05-01\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_localDate_testFail() {
        LocalDate actual = LocalDate.of(2015, Month.MAY, 1);
        LocalDate expected = LocalDate.of(2015, Month.MAY, 2);
        DateTimeAssert.assertAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDate} that is before the
     * {@code expected} {@link LocalDate} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected >\\[2015-05-02\\] but found \\[2015-05-01\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_localDate_testFailNullMessage() {
        LocalDate actual = LocalDate.of(2015, Month.MAY, 1);
        LocalDate expected = LocalDate.of(2015, Month.MAY, 2);
        DateTimeAssert.assertAfter(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDate} that is equal to the
     * {@code expected} {@link LocalDate} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[2015-05-01\\] but found \\[2015-05-01\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_localDate_testFailEqual() {
        LocalDate actual = LocalDate.of(2015, Month.MAY, 1);
        DateTimeAssert.assertAfter(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDate} that is after the
     * {@code expected} {@link LocalDate} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertAfter_localDate_testPass() {
        LocalDate actual = LocalDate.of(2015, Month.MAY, 2);
        LocalDate expected = LocalDate.of(2015, Month.MAY, 1);
        DateTimeAssert.assertAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link LocalDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[null\\] but found \\[2015-05-01T15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_localDateTime_nullExpected() {
        LocalDateTime actual = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        DateTimeAssert.assertAfter(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link LocalDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[2015-05-01T15:15:15\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_localDateTime_nullActual() {
        LocalDateTime expected = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        DateTimeAssert.assertAfter(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link LocalDateTime} and
     * {@code null} {@code actual} {@link LocalDateTime} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_localDateTime_nullExpectedAndActual() {
        DateTimeAssert.assertAfter(
            (LocalDateTime) null,
            (LocalDateTime) null,
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDateTime} that is before the
     * {@code expected} {@link LocalDateTime} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[2015-05-02T15:15:15\\] but found \\[2015-05-01T15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_localDateTime_testFail() {
        LocalDateTime actual = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        LocalDateTime expected = LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15);
        DateTimeAssert.assertAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDateTime} that is before the
     * {@code expected} {@link LocalDateTime} with a {@code null} {@code message}
     * correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected >\\[2015-05-02T15:15:15\\] but found \\[2015-05-01T15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_localDateTime_testFailNullMessage() {
        LocalDateTime actual = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        LocalDateTime expected = LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15);
        DateTimeAssert.assertAfter(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDateTime} that is equal to the
     * {@code expected} {@link LocalDateTime} with a {@code null} {@code message}
     * correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[2015-05-01T15:15:15\\] but found \\[2015-05-01T15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_localDateTime_testFailEqual() {
        LocalDateTime actual = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        DateTimeAssert.assertAfter(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDateTime} that is after the
     * {@code expected} {@link LocalDateTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertAfter_localDateTime_testPass() {
        LocalDateTime actual = LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15);
        LocalDateTime expected = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        DateTimeAssert.assertAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link LocalTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[null\\] but found \\[15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_localTime_nullExpected() {
        LocalTime actual = LocalTime.of(15, 15, 15);
        DateTimeAssert.assertAfter(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link LocalTime} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[15:15:15\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_localTime_nullActual() {
        LocalTime expected = LocalTime.of(15, 15, 15);
        DateTimeAssert.assertAfter(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link LocalTime} and
     * {@code null} {@code actual} {@link LocalTime} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_localTime_nullExpectedAndActual() {
        DateTimeAssert.assertAfter((LocalTime) null, (LocalTime) null, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalTime} that is before the
     * {@code expected} {@link LocalTime} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[16:15:15\\] but found \\[15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_localTime_testFail() {
        LocalTime actual = LocalTime.of(15, 15, 15);
        LocalTime expected = LocalTime.of(16, 15, 15);
        DateTimeAssert.assertAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalTime} that is before the
     * {@code expected} {@link LocalTime} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected >\\[16:15:15\\] but found \\[15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_localTime_testFailNullMessage() {
        LocalTime actual = LocalTime.of(15, 15, 15);
        LocalTime expected = LocalTime.of(16, 15, 15);
        DateTimeAssert.assertAfter(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link LocalTime} that is equal to the
     * {@code expected} {@link LocalTime} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[15:15:15\\] but found \\[15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_localTime_testFailEqual() {
        LocalTime actual = LocalTime.of(15, 15, 15);
        DateTimeAssert.assertAfter(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalTime} that is after the
     * {@code expected} {@link LocalTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertAfter_localTime_testPass() {
        LocalTime actual = LocalTime.of(16, 15, 15);
        LocalTime expected = LocalTime.of(15, 15, 15);
        DateTimeAssert.assertAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link OffsetDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[null\\] but found \\[2015-05-01T15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_offsetDateTime_nullExpected() {
        OffsetDateTime actual = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertAfter(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link OffsetDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[2015-05-01T15:15:15-05:00\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_offsetDateTime_nullActual() {
        OffsetDateTime expected = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertAfter(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link OffsetDateTime} and
     * {@code null} {@code actual} {@link OffsetDateTime} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_offsetDateTime_nullExpectedAndActual() {
        DateTimeAssert.assertAfter(
            (OffsetDateTime) null,
            (OffsetDateTime) null,
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetDateTime} that is before the
     * {@code expected} {@link OffsetDateTime} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[2015-05-02T15:15:15-05:00\\] but found \\[2015-05-01T15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_offsetDateTime_testFail() {
        OffsetDateTime actual = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetDateTime expected = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetDateTime} that is before the
     * {@code expected} {@link OffsetDateTime} with a {@code null} {@code message}
     * correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected >\\[2015-05-02T15:15:15-05:00\\] but found \\[2015-05-01T15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_offsetDateTime_testFailNullMessage() {
        OffsetDateTime actual = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetDateTime expected = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertAfter(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetDateTime} that is equal to the
     * {@code expected} {@link OffsetDateTime} with a {@code null} {@code message}
     * correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[2015-05-01T15:15:15-05:00\\] but found \\[2015-05-01T15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_offsetDateTime_testFailEqual() {
        OffsetDateTime actual = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertAfter(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetDateTime} that is after the
     * {@code expected} {@link OffsetDateTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertAfter_offsetDateTime_testPass() {
        OffsetDateTime actual = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetDateTime expected = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link OffsetTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[null\\] but found \\[15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_offsetTime_nullExpected() {
        OffsetTime actual = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertAfter(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link OffsetTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[15:15:15-05:00\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_offsetTime_nullActual() {
        OffsetTime expected = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertAfter(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link OffsetTime} and
     * {@code null} {@code actual} {@link OffsetTime} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_offsetTime_nullExpectedAndActual() {
        DateTimeAssert.assertAfter((OffsetTime) null, (OffsetTime) null, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetTime} that is before the
     * {@code expected} {@link OffsetTime} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[16:15:15-05:00\\] but found \\[15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_offsetTime_testFail() {
        OffsetTime actual = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetTime expected = OffsetTime.of(
            LocalTime.of(16, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetTime} that is before the
     * {@code expected} {@link OffsetTime} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected >\\[16:15:15-05:00\\] but found \\[15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_offsetTime_testFailNullMessage() {
        OffsetTime actual = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetTime expected = OffsetTime.of(
            LocalTime.of(16, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertAfter(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetTime} that is equal to the
     * {@code expected} {@link OffsetTime} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[15:15:15-05:00\\] but found \\[15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_offsetTime_testFailEqual() {
        OffsetTime actual = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertAfter(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetTime} that is after the
     * {@code expected} {@link OffsetTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertAfter_offsetTime_testPass() {
        OffsetTime actual = OffsetTime.of(
            LocalTime.of(16, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetTime expected = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link ZonedDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[null\\] but found \\[2015-05-01T15:15:15-05:00\\[America/Winnipeg\\]\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_zonedDateTime_nullExpected() {
        ZonedDateTime actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertAfter(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link ZonedDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[2015-05-01T15:15:15-05:00\\[America/Winnipeg\\]\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_zonedDateTime_nullActual() {
        ZonedDateTime expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertAfter(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link ZonedDateTime} and
     * {@code null} {@code actual} {@link ZonedDateTime} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_zonedDateTime_nullExpectedAndActual() {
        DateTimeAssert.assertAfter(
            (ZonedDateTime) null,
            (ZonedDateTime) null,
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link ZonedDateTime} that is before the
     * {@code expected} {@link ZonedDateTime} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[2015-05-02T15:15:15-05:00\\[America/Winnipeg\\]\\] but found \\[2015-05-01T15:15:15-05:00\\[America/Winnipeg\\]\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_zonedDateTime_testFail() {
        ZonedDateTime actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        ZonedDateTime expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link ZonedDateTime} that is before the
     * {@code expected} {@link ZonedDateTime} with a {@code null} {@code message}
     * correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected >\\[2015-05-02T15:15:15-05:00\\[America/Winnipeg\\]\\] but found \\[2015-05-01T15:15:15-05:00\\[America/Winnipeg\\]\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_zonedDateTime_testFailNullMessage() {
        ZonedDateTime actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        ZonedDateTime expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertAfter(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link ZonedDateTime} that is equal to the
     * {@code expected} {@link ZonedDateTime} with a {@code null} {@code message}
     * correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >\\[2015-05-01T15:15:15-05:00\\[America/Winnipeg\\]\\] but found \\[2015-05-01T15:15:15-05:00\\[America/Winnipeg\\]\\]",
        groups = { "unit-tests" })
    public static void testAssertAfter_zonedDateTime_testFailEqual() {
        ZonedDateTime actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertAfter(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link ZonedDateTime} that is after the
     * {@code expected} {@link ZonedDateTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertAfter_zonedDateTime_testPass() {
        ZonedDateTime actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        ZonedDateTime expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link Calendar} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[null\\] but found \\[Fri May 01 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_calendar_nullExpected() {
        Calendar actual = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertBefore(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link Calendar} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[Fri May 01 15:15:15 CDT 2015\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_calendar_nullActual() {
        Calendar expected = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertBefore(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link Calendar} and
     * {@code null} {@code actual} {@link Calendar} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_calendar_nullExpectedAndActual() {
        DateTimeAssert.assertBefore((Calendar) null, (Calendar) null, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Calendar} that is after the
     * {@code expected} {@link Calendar} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[Fri May 01 15:15:15 CDT 2015\\] but found \\[Sat May 02 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_calendar_testFail() {
        Calendar actual = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        Calendar expected = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Calendar} that is after the
     * {@code expected} {@link Calendar} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected <\\[Fri May 01 15:15:15 CDT 2015\\] but found \\[Sat May 02 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_calendar_testFailNullMessage() {
        Calendar actual = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        Calendar expected = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertBefore(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link Calendar} that is equal to the
     * {@code expected} {@link Calendar} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[Fri May 01 15:15:15 CDT 2015\\] but found \\[Fri May 01 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_calendar_testFailEqual() {
        Calendar actual = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertBefore(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Calendar} that is before the
     * {@code expected} {@link Calendar} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertBefore_calendar_testPass() {
        Calendar actual = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        Calendar expected = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link Date} correctly throws an
     * {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[null\\] but found \\[Fri May 01 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_date_nullExpected() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertBefore(Date.from(actual), null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link Date} correctly throws an
     * {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[Fri May 01 15:15:15 CDT 2015\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_date_nullActual() {
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertBefore(null, Date.from(expected), "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link Date} and
     * {@code null} {@code actual} {@link Date} correctly throws an {@link AssertionError}
     * .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_date_nullExpectedAndActual() {
        DateTimeAssert.assertBefore((Date) null, (Date) null, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Date} that is after the
     * {@code expected} {@link Date} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[Fri May 01 15:15:15 CDT 2015\\] but found \\[Sat May 02 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_date_testFail() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertBefore(
            Date.from(actual),
            Date.from(expected),
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Date} that is after the
     * {@code expected} {@link Date} with a {@code null} {@code message} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected <\\[Fri May 01 15:15:15 CDT 2015\\] but found \\[Sat May 02 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_date_testFailNullMessage() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertBefore(Date.from(actual), Date.from(expected), null);
    }

    /**
     * Tests that passing an {@code actual} {@link Date} that is equal to the
     * {@code expected} {@link Date} with a {@code null} {@code message} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[Fri May 01 15:15:15 CDT 2015\\] but found \\[Fri May 01 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_date_testFailEqual() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertBefore(Date.from(actual), Date.from(actual), "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Date} that is before the
     * {@code expected} {@link Date} correctly does not throw an {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertBefore_date_testPass() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertBefore(
            Date.from(actual),
            Date.from(expected),
            "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link Instant} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[null\\] but found \\[2015-05-01T20:15:15Z\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_instant_nullExpected() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertBefore(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link Instant} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[2015-05-01T20:15:15Z\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_instant_nullActual() {
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertBefore(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link Instant} and
     * {@code null} {@code actual} {@link Instant} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_instant_nullExpectedAndActual() {
        DateTimeAssert.assertBefore((Instant) null, (Instant) null, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Instant} that is after the
     * {@code expected} {@link Instant} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[2015-05-01T20:15:15Z\\] but found \\[2015-05-02T20:15:15Z\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_instant_testFail() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Instant} that is after the
     * {@code expected} {@link Instant} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected <\\[2015-05-01T20:15:15Z\\] but found \\[2015-05-02T20:15:15Z\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_instant_testFailNullMessage() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertBefore(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link Instant} that is equal to the
     * {@code expected} {@link Instant} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[2015-05-01T20:15:15Z\\] but found \\[2015-05-01T20:15:15Z\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_instant_testFailEqual() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertBefore(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Instant} that is before the
     * {@code expected} {@link Instant} correctly does not throw an {@link AssertionError}
     * .
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertBefore_instant_testPass() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link LocalDate} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[null\\] but found \\[2015-05-01\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_localDate_nullExpected() {
        LocalDate actual = LocalDate.of(2015, Month.MAY, 1);
        DateTimeAssert.assertBefore(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link LocalDate} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[2015-05-01\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_localDate_nullActual() {
        LocalDate expected = LocalDate.of(2015, Month.MAY, 1);
        DateTimeAssert.assertBefore(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link LocalDate} and
     * {@code null} {@code actual} {@link LocalDate} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_localDate_nullExpectedAndActual() {
        DateTimeAssert.assertBefore((LocalDate) null, (LocalDate) null, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDate} that is after the
     * {@code expected} {@link LocalDate} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[2015-05-01\\] but found \\[2015-05-02\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_localDate_testFail() {
        LocalDate actual = LocalDate.of(2015, Month.MAY, 2);
        LocalDate expected = LocalDate.of(2015, Month.MAY, 1);
        DateTimeAssert.assertBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDate} that is after the
     * {@code expected} {@link LocalDate} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected <\\[2015-05-01\\] but found \\[2015-05-02\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_localDate_testFailNullMessage() {
        LocalDate actual = LocalDate.of(2015, Month.MAY, 2);
        LocalDate expected = LocalDate.of(2015, Month.MAY, 1);
        DateTimeAssert.assertBefore(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDate} that is equal to the
     * {@code expected} {@link LocalDate} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[2015-05-01\\] but found \\[2015-05-01\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_localDate_testFailEqual() {
        LocalDate actual = LocalDate.of(2015, Month.MAY, 1);
        DateTimeAssert.assertBefore(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDate} that is before the
     * {@code expected} {@link LocalDate} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertBefore_localDate_testPass() {
        LocalDate actual = LocalDate.of(2015, Month.MAY, 1);
        LocalDate expected = LocalDate.of(2015, Month.MAY, 2);
        DateTimeAssert.assertBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link LocalDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[null\\] but found \\[2015-05-01T15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_localDateTime_nullExpected() {
        LocalDateTime actual = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        DateTimeAssert.assertBefore(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link LocalDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[2015-05-01T15:15:15\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_localDateTime_nullActual() {
        LocalDateTime expected = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        DateTimeAssert.assertBefore(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link LocalDateTime} and
     * {@code null} {@code actual} {@link LocalDateTime} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_localDateTime_nullExpectedAndActual() {
        DateTimeAssert.assertBefore(
            (LocalDateTime) null,
            (LocalDateTime) null,
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDateTime} that is after the
     * {@code expected} {@link LocalDateTime} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[2015-05-01T15:15:15\\] but found \\[2015-05-02T15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_localDateTime_testFail() {
        LocalDateTime actual = LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15);
        LocalDateTime expected = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        DateTimeAssert.assertBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDateTime} that is after the
     * {@code expected} {@link LocalDateTime} with a {@code null} {@code message}
     * correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected <\\[2015-05-01T15:15:15\\] but found \\[2015-05-02T15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_localDateTime_testFailNullMessage() {
        LocalDateTime actual = LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15);
        LocalDateTime expected = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        DateTimeAssert.assertBefore(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDateTime} that is equal to the
     * {@code expected} {@link LocalDateTime} with a {@code null} {@code message}
     * correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[2015-05-01T15:15:15\\] but found \\[2015-05-01T15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_localDateTime_testFailEqual() {
        LocalDateTime actual = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        DateTimeAssert.assertBefore(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDateTime} that is before the
     * {@code expected} {@link LocalDateTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertBefore_localDateTime_testPass() {
        LocalDateTime actual = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        LocalDateTime expected = LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15);
        DateTimeAssert.assertBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link LocalTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[null\\] but found \\[15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_localTime_nullExpected() {
        LocalTime actual = LocalTime.of(15, 15, 15);
        DateTimeAssert.assertBefore(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link LocalTime} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[15:15:15\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_localTime_nullActual() {
        LocalTime expected = LocalTime.of(15, 15, 15);
        DateTimeAssert.assertBefore(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link LocalTime} and
     * {@code null} {@code actual} {@link LocalTime} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_localTime_nullExpectedAndActual() {
        DateTimeAssert.assertBefore((LocalTime) null, (LocalTime) null, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalTime} that is after the
     * {@code expected} {@link LocalTime} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[15:15:15\\] but found \\[16:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_localTime_testFail() {
        LocalTime actual = LocalTime.of(16, 15, 15);
        LocalTime expected = LocalTime.of(15, 15, 15);
        DateTimeAssert.assertBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalTime} that is after the
     * {@code expected} {@link LocalTime} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected <\\[15:15:15\\] but found \\[16:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_localTime_testFailNullMessage() {
        LocalTime actual = LocalTime.of(16, 15, 15);
        LocalTime expected = LocalTime.of(15, 15, 15);
        DateTimeAssert.assertBefore(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link LocalTime} that is equal to the
     * {@code expected} {@link LocalTime} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[15:15:15\\] but found \\[15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_localTime_testFailEqual() {
        LocalTime actual = LocalTime.of(15, 15, 15);
        DateTimeAssert.assertBefore(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalTime} that is before the
     * {@code expected} {@link LocalTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertBefore_localTime_testPass() {
        LocalTime actual = LocalTime.of(15, 15, 15);
        LocalTime expected = LocalTime.of(16, 15, 15);
        DateTimeAssert.assertBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link OffsetDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[null\\] but found \\[2015-05-01T15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_offsetDateTime_nullExpected() {
        OffsetDateTime actual = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertBefore(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link OffsetDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[2015-05-01T15:15:15-05:00\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_offsetDateTime_nullActual() {
        OffsetDateTime expected = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertBefore(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link OffsetDateTime} and
     * {@code null} {@code actual} {@link OffsetDateTime} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_offsetDateTime_nullExpectedAndActual() {
        DateTimeAssert.assertBefore(
            (OffsetDateTime) null,
            (OffsetDateTime) null,
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetDateTime} that is after the
     * {@code expected} {@link OffsetDateTime} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[2015-05-01T15:15:15-05:00\\] but found \\[2015-05-02T15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_offsetDateTime_testFail() {
        OffsetDateTime actual = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetDateTime expected = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetDateTime} that is after the
     * {@code expected} {@link OffsetDateTime} with a {@code null} {@code message}
     * correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected <\\[2015-05-01T15:15:15-05:00\\] but found \\[2015-05-02T15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_offsetDateTime_testFailNullMessage() {
        OffsetDateTime actual = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetDateTime expected = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertBefore(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetDateTime} that is equal to the
     * {@code expected} {@link OffsetDateTime} with a {@code null} {@code message}
     * correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[2015-05-01T15:15:15-05:00\\] but found \\[2015-05-01T15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_offsetDateTime_testFailEqual() {
        OffsetDateTime actual = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertBefore(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetDateTime} that is before the
     * {@code expected} {@link OffsetDateTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertBefore_offsetDateTime_testPass() {
        OffsetDateTime actual = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetDateTime expected = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link OffsetTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[null\\] but found \\[15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_offsetTime_nullExpected() {
        OffsetTime actual = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertBefore(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link OffsetTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[15:15:15-05:00\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_offsetTime_nullActual() {
        OffsetTime expected = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertBefore(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link OffsetTime} and
     * {@code null} {@code actual} {@link OffsetTime} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_offsetTime_nullExpectedAndActual() {
        DateTimeAssert.assertBefore((OffsetTime) null, (OffsetTime) null, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetTime} that is after the
     * {@code expected} {@link OffsetTime} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[15:15:15-05:00\\] but found \\[16:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_offsetTime_testFail() {
        OffsetTime actual = OffsetTime.of(
            LocalTime.of(16, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetTime expected = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetTime} that is after the
     * {@code expected} {@link OffsetTime} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected <\\[15:15:15-05:00\\] but found \\[16:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_offsetTime_testFailNullMessage() {
        OffsetTime actual = OffsetTime.of(
            LocalTime.of(16, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetTime expected = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertBefore(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetTime} that is equal to the
     * {@code expected} {@link OffsetTime} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[15:15:15-05:00\\] but found \\[15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_offsetTime_testFailEqual() {
        OffsetTime actual = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertBefore(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetTime} that is before the
     * {@code expected} {@link OffsetTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertBefore_offsetTime_testPass() {
        OffsetTime actual = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetTime expected = OffsetTime.of(
            LocalTime.of(16, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link ZonedDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[null\\] but found \\[2015-05-01T15:15:15-05:00\\[America/Winnipeg\\]\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_zonedDateTime_nullExpected() {
        ZonedDateTime actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertBefore(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link ZonedDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[2015-05-01T15:15:15-05:00\\[America/Winnipeg\\]\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_zonedDateTime_nullActual() {
        ZonedDateTime expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertBefore(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link ZonedDateTime} and
     * {@code null} {@code actual} {@link ZonedDateTime} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_zonedDateTime_nullExpectedAndActual() {
        DateTimeAssert.assertBefore(
            (ZonedDateTime) null,
            (ZonedDateTime) null,
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link ZonedDateTime} that is after the
     * {@code expected} {@link ZonedDateTime} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[2015-05-01T15:15:15-05:00\\[America/Winnipeg\\]\\] but found \\[2015-05-02T15:15:15-05:00\\[America/Winnipeg\\]\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_zonedDateTime_testFail() {
        ZonedDateTime actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        ZonedDateTime expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link ZonedDateTime} that is after the
     * {@code expected} {@link ZonedDateTime} with a {@code null} {@code message}
     * correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected <\\[2015-05-01T15:15:15-05:00\\[America/Winnipeg\\]\\] but found \\[2015-05-02T15:15:15-05:00\\[America/Winnipeg\\]\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_zonedDateTime_testFailNullMessage() {
        ZonedDateTime actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        ZonedDateTime expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertBefore(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link ZonedDateTime} that is equal to the
     * {@code expected} {@link ZonedDateTime} with a {@code null} {@code message}
     * correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <\\[2015-05-01T15:15:15-05:00\\[America/Winnipeg\\]\\] but found \\[2015-05-01T15:15:15-05:00\\[America/Winnipeg\\]\\]",
        groups = { "unit-tests" })
    public static void testAssertBefore_zonedDateTime_testFailEqual() {
        ZonedDateTime actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertBefore(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link ZonedDateTime} that is before the
     * {@code expected} {@link ZonedDateTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertBefore_zonedDateTime_testPass() {
        ZonedDateTime actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        ZonedDateTime expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link Calendar} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[null\\] but found \\[Fri May 01 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_calendar_nullExpected() {
        Calendar actual = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertNotAfter(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link Calendar} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[Fri May 01 15:15:15 CDT 2015\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_calendar_nullActual() {
        Calendar expected = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertNotAfter(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link Calendar} and
     * {@code null} {@code actual} {@link Calendar} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_calendar_nullExpectedAndActual() {
        DateTimeAssert.assertNotAfter((Calendar) null, (Calendar) null, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Calendar} that is after the
     * {@code expected} {@link Calendar} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[Fri May 01 15:15:15 CDT 2015\\] but found \\[Sat May 02 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_calendar_testFail() {
        Calendar actual = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        Calendar expected = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertNotAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Calendar} that is after the
     * {@code expected} {@link Calendar} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected <=\\[Fri May 01 15:15:15 CDT 2015\\] but found \\[Sat May 02 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_calendar_testFailNullMessage() {
        Calendar actual = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        Calendar expected = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertNotAfter(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link Calendar} that is equal to the
     * {@code expected} {@link Calendar} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotAfter_calendar_testPassEqual() {
        Calendar actual = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertNotAfter(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Calendar} that is not after the
     * {@code expected} {@link Calendar} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotAfter_calendar_testPass() {
        Calendar actual = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        Calendar expected = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertNotAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link Date} correctly throws an
     * {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[null\\] but found \\[Fri May 01 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_date_nullExpected() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotAfter(Date.from(actual), null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link Date} correctly throws an
     * {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[Fri May 01 15:15:15 CDT 2015\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_date_nullActual() {
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotAfter(null, Date.from(expected), "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link Date} and
     * {@code null} {@code actual} {@link Date} correctly throws an {@link AssertionError}
     * .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_date_nullExpectedAndActual() {
        DateTimeAssert.assertNotAfter((Date) null, (Date) null, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Date} that is after the
     * {@code expected} {@link Date} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[Fri May 01 15:15:15 CDT 2015\\] but found \\[Sat May 02 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_date_testFail() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotAfter(
            Date.from(actual),
            Date.from(expected),
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Date} that is after the
     * {@code expected} {@link Date} with a {@code null} {@code message} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected <=\\[Fri May 01 15:15:15 CDT 2015\\] but found \\[Sat May 02 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_date_testFailNullMessage() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotAfter(Date.from(actual), Date.from(expected), null);
    }

    /**
     * Tests that passing an {@code actual} {@link Date} that is equal to the
     * {@code expected} {@link Date} correctly does not throw an {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotAfter_date_testPassEqual() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotAfter(
            Date.from(actual),
            Date.from(actual),
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Date} that is not after the
     * {@code expected} {@link Date} correctly does not throw an {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotAfter_date_testPass() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotAfter(
            Date.from(actual),
            Date.from(expected),
            "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link Instant} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[null\\] but found \\[2015-05-01T20:15:15Z\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_instant_nullExpected() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotAfter(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link Instant} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[2015-05-01T20:15:15Z\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_instant_nullActual() {
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotAfter(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link Instant} and
     * {@code null} {@code actual} {@link Instant} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_instant_nullExpectedAndActual() {
        DateTimeAssert.assertNotAfter((Instant) null, (Instant) null, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Instant} that is after the
     * {@code expected} {@link Instant} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[2015-05-01T20:15:15Z\\] but found \\[2015-05-02T20:15:15Z\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_instant_testFail() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Instant} that is after the
     * {@code expected} {@link Instant} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected <=\\[2015-05-01T20:15:15Z\\] but found \\[2015-05-02T20:15:15Z\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_instant_testFailNullMessage() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotAfter(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link Instant} that is equal to the
     * {@code expected} {@link Instant} correctly does not throw an {@link AssertionError}
     * .
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotAfter_instant_testPassEqual() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotAfter(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Instant} that is not after the
     * {@code expected} {@link Instant} correctly does not throw an {@link AssertionError}
     * .
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotAfter_instant_testPass() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link LocalDate} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[null\\] but found \\[2015-05-01\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_localDate_nullExpected() {
        LocalDate actual = LocalDate.of(2015, Month.MAY, 1);
        DateTimeAssert.assertNotAfter(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link LocalDate} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[2015-05-01\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_localDate_nullActual() {
        LocalDate expected = LocalDate.of(2015, Month.MAY, 1);
        DateTimeAssert.assertNotAfter(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link LocalDate} and
     * {@code null} {@code actual} {@link LocalDate} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_localDate_nullExpectedAndActual() {
        DateTimeAssert.assertNotAfter((LocalDate) null, (LocalDate) null, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDate} that is after the
     * {@code expected} {@link LocalDate} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[2015-05-01\\] but found \\[2015-05-02\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_localDate_testFail() {
        LocalDate actual = LocalDate.of(2015, Month.MAY, 2);
        LocalDate expected = LocalDate.of(2015, Month.MAY, 1);
        DateTimeAssert.assertNotAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDate} that is after the
     * {@code expected} {@link LocalDate} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected <=\\[2015-05-01\\] but found \\[2015-05-02\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_localDate_testFailNullMessage() {
        LocalDate actual = LocalDate.of(2015, Month.MAY, 2);
        LocalDate expected = LocalDate.of(2015, Month.MAY, 1);
        DateTimeAssert.assertNotAfter(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDate} that is equal to the
     * {@code expected} {@link LocalDate} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotAfter_localDate_testPassEqual() {
        LocalDate actual = LocalDate.of(2015, Month.MAY, 1);
        DateTimeAssert.assertNotAfter(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDate} that is not after the
     * {@code expected} {@link LocalDate} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotAfter_localDate_testPass() {
        LocalDate actual = LocalDate.of(2015, Month.MAY, 1);
        LocalDate expected = LocalDate.of(2015, Month.MAY, 2);
        DateTimeAssert.assertNotAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link LocalDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[null\\] but found \\[2015-05-01T15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_localDateTime_nullExpected() {
        LocalDateTime actual = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        DateTimeAssert.assertNotAfter(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link LocalDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[2015-05-01T15:15:15\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_localDateTime_nullActual() {
        LocalDateTime expected = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        DateTimeAssert.assertNotAfter(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link LocalDateTime} and
     * {@code null} {@code actual} {@link LocalDateTime} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_localDateTime_nullExpectedAndActual() {
        DateTimeAssert.assertNotAfter(
            (LocalDateTime) null,
            (LocalDateTime) null,
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDateTime} that is after the
     * {@code expected} {@link LocalDateTime} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[2015-05-01T15:15:15\\] but found \\[2015-05-02T15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_localDateTime_testFail() {
        LocalDateTime actual = LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15);
        LocalDateTime expected = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        DateTimeAssert.assertNotAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDateTime} that is after the
     * {@code expected} {@link LocalDateTime} with a {@code null} {@code message}
     * correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected <=\\[2015-05-01T15:15:15\\] but found \\[2015-05-02T15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_localDateTime_testFailNullMessage() {
        LocalDateTime actual = LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15);
        LocalDateTime expected = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        DateTimeAssert.assertNotAfter(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDateTime} that is equal to the
     * {@code expected} {@link LocalDateTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotAfter_localDateTime_testPassEqual() {
        LocalDateTime actual = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        DateTimeAssert.assertNotAfter(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDateTime} that is not after the
     * {@code expected} {@link LocalDateTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotAfter_localDateTime_testPass() {
        LocalDateTime actual = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        LocalDateTime expected = LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15);
        DateTimeAssert.assertNotAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link LocalTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[null\\] but found \\[15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_localTime_nullExpected() {
        LocalTime actual = LocalTime.of(15, 15, 15);
        DateTimeAssert.assertNotAfter(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link LocalTime} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[15:15:15\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_localTime_nullActual() {
        LocalTime expected = LocalTime.of(15, 15, 15);
        DateTimeAssert.assertNotAfter(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link LocalTime} and
     * {@code null} {@code actual} {@link LocalTime} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_localTime_nullExpectedAndActual() {
        DateTimeAssert.assertNotAfter((LocalTime) null, (LocalTime) null, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalTime} that is after the
     * {@code expected} {@link LocalTime} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[15:15:15\\] but found \\[16:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_localTime_testFail() {
        LocalTime actual = LocalTime.of(16, 15, 15);
        LocalTime expected = LocalTime.of(15, 15, 15);
        DateTimeAssert.assertNotAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalTime} that is after the
     * {@code expected} {@link LocalTime} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected <=\\[15:15:15\\] but found \\[16:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_localTime_testFailNullMessage() {
        LocalTime actual = LocalTime.of(16, 15, 15);
        LocalTime expected = LocalTime.of(15, 15, 15);
        DateTimeAssert.assertNotAfter(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link LocalTime} that is equal to the
     * {@code expected} {@link LocalTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotAfter_localTime_testPassEqual() {
        LocalTime actual = LocalTime.of(15, 15, 15);
        DateTimeAssert.assertNotAfter(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalTime} that is not after the
     * {@code expected} {@link LocalTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotAfter_localTime_testPass() {
        LocalTime actual = LocalTime.of(15, 15, 15);
        LocalTime expected = LocalTime.of(16, 15, 15);
        DateTimeAssert.assertNotAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link OffsetDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[null\\] but found \\[2015-05-01T15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_offsetDateTime_nullExpected() {
        OffsetDateTime actual = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotAfter(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link OffsetDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[2015-05-01T15:15:15-05:00\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_offsetDateTime_nullActual() {
        OffsetDateTime expected = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotAfter(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link OffsetDateTime} and
     * {@code null} {@code actual} {@link OffsetDateTime} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_offsetDateTime_nullExpectedAndActual() {
        DateTimeAssert.assertNotAfter(
            (OffsetDateTime) null,
            (OffsetDateTime) null,
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetDateTime} that is after the
     * {@code expected} {@link OffsetDateTime} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[2015-05-01T15:15:15-05:00\\] but found \\[2015-05-02T15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_offsetDateTime_testFail() {
        OffsetDateTime actual = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetDateTime expected = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetDateTime} that is after the
     * {@code expected} {@link OffsetDateTime} with a {@code null} {@code message}
     * correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected <=\\[2015-05-01T15:15:15-05:00\\] but found \\[2015-05-02T15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_offsetDateTime_testFailNullMessage() {
        OffsetDateTime actual = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetDateTime expected = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotAfter(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetDateTime} that is equal to the
     * {@code expected} {@link OffsetDateTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotAfter_offsetDateTime_testPassEqual() {
        OffsetDateTime actual = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotAfter(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetDateTime} that is not after the
     * {@code expected} {@link OffsetDateTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotAfter_offsetDateTime_testPass() {
        OffsetDateTime actual = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetDateTime expected = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link OffsetTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[null\\] but found \\[15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_offsetTime_nullExpected() {
        OffsetTime actual = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotAfter(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link OffsetTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[15:15:15-05:00\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_offsetTime_nullActual() {
        OffsetTime expected = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotAfter(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link OffsetTime} and
     * {@code null} {@code actual} {@link OffsetTime} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_offsetTime_nullExpectedAndActual() {
        DateTimeAssert.assertNotAfter(
            (OffsetTime) null,
            (OffsetTime) null,
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetTime} that is after the
     * {@code expected} {@link OffsetTime} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[15:15:15-05:00\\] but found \\[16:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_offsetTime_testFail() {
        OffsetTime actual = OffsetTime.of(
            LocalTime.of(16, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetTime expected = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetTime} that is after the
     * {@code expected} {@link OffsetTime} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected <=\\[15:15:15-05:00\\] but found \\[16:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_offsetTime_testFailNullMessage() {
        OffsetTime actual = OffsetTime.of(
            LocalTime.of(16, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetTime expected = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotAfter(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetTime} that is equal to the
     * {@code expected} {@link OffsetTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotAfter_offsetTime_testPassEqual() {
        OffsetTime actual = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotAfter(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetTime} that is not after the
     * {@code expected} {@link OffsetTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotAfter_offsetTime_testPass() {
        OffsetTime actual = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetTime expected = OffsetTime.of(
            LocalTime.of(16, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link ZonedDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[null\\] but found \\[2015-05-01T15:15:15-05:00\\[America/Winnipeg\\]\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_zonedDateTime_nullExpected() {
        ZonedDateTime actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertNotAfter(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link ZonedDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[2015-05-01T15:15:15-05:00\\[America/Winnipeg\\]\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_zonedDateTime_nullActual() {
        ZonedDateTime expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertNotAfter(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link ZonedDateTime} and
     * {@code null} {@code actual} {@link ZonedDateTime} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_zonedDateTime_nullExpectedAndActual() {
        DateTimeAssert.assertNotAfter(
            (ZonedDateTime) null,
            (ZonedDateTime) null,
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link ZonedDateTime} that is after the
     * {@code expected} {@link ZonedDateTime} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected <=\\[2015-05-01T15:15:15-05:00\\[America/Winnipeg\\]\\] but found \\[2015-05-02T15:15:15-05:00\\[America/Winnipeg\\]\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_zonedDateTime_testFail() {
        ZonedDateTime actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        ZonedDateTime expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertNotAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link ZonedDateTime} that is after the
     * {@code expected} {@link ZonedDateTime} with a {@code null} {@code message}
     * correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected <=\\[2015-05-01T15:15:15-05:00\\[America/Winnipeg\\]\\] but found \\[2015-05-02T15:15:15-05:00\\[America/Winnipeg\\]\\]",
        groups = { "unit-tests" })
    public static void testAssertNotAfter_zonedDateTime_testFailNullMessage() {
        ZonedDateTime actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        ZonedDateTime expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertNotAfter(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link ZonedDateTime} that is equal to the
     * {@code expected} {@link ZonedDateTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotAfter_zonedDateTime_testPassEqual() {
        ZonedDateTime actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertNotAfter(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link ZonedDateTime} that is not after the
     * {@code expected} {@link ZonedDateTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotAfter_zonedDateTime_testPass() {
        ZonedDateTime actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        ZonedDateTime expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertNotAfter(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link Calendar} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[null\\] but found \\[Fri May 01 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_calendar_nullExpected() {
        Calendar actual = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertNotBefore(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link Calendar} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[Fri May 01 15:15:15 CDT 2015\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_calendar_nullActual() {
        Calendar expected = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertNotBefore(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link Calendar} and
     * {@code null} {@code actual} {@link Calendar} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_calendar_nullExpectedAndActual() {
        DateTimeAssert.assertNotBefore((Calendar) null, (Calendar) null, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Calendar} that is before the
     * {@code expected} {@link Calendar} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[Sat May 02 15:15:15 CDT 2015\\] but found \\[Fri May 01 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_calendar_testFail() {
        Calendar actual = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        Calendar expected = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertNotBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Calendar} that is before the
     * {@code expected} {@link Calendar} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected >=\\[Sat May 02 15:15:15 CDT 2015\\] but found \\[Fri May 01 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_calendar_testFailNullMessage() {
        Calendar actual = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        Calendar expected = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertNotBefore(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link Calendar} that is equal to the
     * {@code expected} {@link Calendar} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotBefore_calendar_testPassEqual() {
        Calendar actual = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertNotBefore(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Calendar} that is not before the
     * {@code expected} {@link Calendar} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotBefore_calendar_testPass() {
        Calendar actual = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        Calendar expected = GregorianCalendar.from(
            ZonedDateTime.of(
                LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
                ZoneId.of("America/Winnipeg")));
        DateTimeAssert.assertNotBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link Date} correctly throws an
     * {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[null\\] but found \\[Fri May 01 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_date_nullExpected() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotBefore(Date.from(actual), null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link Date} correctly throws an
     * {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[Fri May 01 15:15:15 CDT 2015\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_date_nullActual() {
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotBefore(null, Date.from(expected), "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link Date} and
     * {@code null} {@code actual} {@link Date} correctly throws an {@link AssertionError}
     * .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_date_nullExpectedAndActual() {
        DateTimeAssert.assertNotBefore((Date) null, (Date) null, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Date} that is before the
     * {@code expected} {@link Date} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[Sat May 02 15:15:15 CDT 2015\\] but found \\[Fri May 01 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_date_testFail() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotBefore(
            Date.from(actual),
            Date.from(expected),
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Date} that is before the
     * {@code expected} {@link Date} with a {@code null} {@code message} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected >=\\[Sat May 02 15:15:15 CDT 2015\\] but found \\[Fri May 01 15:15:15 CDT 2015\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_date_testFailNullMessage() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotBefore(Date.from(actual), Date.from(expected), null);
    }

    /**
     * Tests that passing an {@code actual} {@link Date} that is equal to the
     * {@code expected} {@link Date} correctly does not throw an {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotBefore_date_testPassEqual() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotBefore(
            Date.from(actual),
            Date.from(actual),
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Date} that is not before the
     * {@code expected} {@link Date} correctly does not throw an {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotBefore_date_testPass() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotBefore(
            Date.from(actual),
            Date.from(expected),
            "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link Instant} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[null\\] but found \\[2015-05-01T20:15:15Z\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_instant_nullExpected() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotBefore(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link Instant} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[2015-05-01T20:15:15Z\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_instant_nullActual() {
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotBefore(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link Instant} and
     * {@code null} {@code actual} {@link Instant} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_instant_nullExpectedAndActual() {
        DateTimeAssert.assertNotBefore((Instant) null, (Instant) null, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Instant} that is before the
     * {@code expected} {@link Instant} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[2015-05-02T20:15:15Z\\] but found \\[2015-05-01T20:15:15Z\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_instant_testFail() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Instant} that is before the
     * {@code expected} {@link Instant} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected >=\\[2015-05-02T20:15:15Z\\] but found \\[2015-05-01T20:15:15Z\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_instant_testFailNullMessage() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotBefore(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link Instant} that is equal to the
     * {@code expected} {@link Instant} correctly does not throw an {@link AssertionError}
     * .
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotBefore_instant_testPassEqual() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotBefore(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link Instant} that is not before the
     * {@code expected} {@link Instant} correctly does not throw an {@link AssertionError}
     * .
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotBefore_instant_testPass() {
        Instant actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        Instant expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg")).toInstant();
        DateTimeAssert.assertNotBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link LocalDate} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[null\\] but found \\[2015-05-01\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_localDate_nullExpected() {
        LocalDate actual = LocalDate.of(2015, Month.MAY, 1);
        DateTimeAssert.assertNotBefore(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link LocalDate} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[2015-05-01\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_localDate_nullActual() {
        LocalDate expected = LocalDate.of(2015, Month.MAY, 1);
        DateTimeAssert.assertNotBefore(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link LocalDate} and
     * {@code null} {@code actual} {@link LocalDate} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_localDate_nullExpectedAndActual() {
        DateTimeAssert.assertNotBefore(
            (LocalDate) null,
            (LocalDate) null,
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDate} that is before the
     * {@code expected} {@link LocalDate} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[2015-05-02\\] but found \\[2015-05-01\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_localDate_testFail() {
        LocalDate actual = LocalDate.of(2015, Month.MAY, 1);
        LocalDate expected = LocalDate.of(2015, Month.MAY, 2);
        DateTimeAssert.assertNotBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDate} that is before the
     * {@code expected} {@link LocalDate} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected >=\\[2015-05-02\\] but found \\[2015-05-01\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_localDate_testFailNullMessage() {
        LocalDate actual = LocalDate.of(2015, Month.MAY, 1);
        LocalDate expected = LocalDate.of(2015, Month.MAY, 2);
        DateTimeAssert.assertNotBefore(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDate} that is equal to the
     * {@code expected} {@link LocalDate} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotBefore_localDate_testPassEqual() {
        LocalDate actual = LocalDate.of(2015, Month.MAY, 1);
        DateTimeAssert.assertNotBefore(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDate} that is not before the
     * {@code expected} {@link LocalDate} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotBefore_localDate_testPass() {
        LocalDate actual = LocalDate.of(2015, Month.MAY, 2);
        LocalDate expected = LocalDate.of(2015, Month.MAY, 1);
        DateTimeAssert.assertNotBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link LocalDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[null\\] but found \\[2015-05-01T15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_localDateTime_nullExpected() {
        LocalDateTime actual = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        DateTimeAssert.assertNotBefore(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link LocalDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[2015-05-01T15:15:15\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_localDateTime_nullActual() {
        LocalDateTime expected = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        DateTimeAssert.assertNotBefore(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link LocalDateTime} and
     * {@code null} {@code actual} {@link LocalDateTime} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_localDateTime_nullExpectedAndActual() {
        DateTimeAssert.assertNotBefore(
            (LocalDateTime) null,
            (LocalDateTime) null,
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDateTime} that is before the
     * {@code expected} {@link LocalDateTime} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[2015-05-02T15:15:15\\] but found \\[2015-05-01T15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_localDateTime_testFail() {
        LocalDateTime actual = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        LocalDateTime expected = LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15);
        DateTimeAssert.assertNotBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDateTime} that is before the
     * {@code expected} {@link LocalDateTime} with a {@code null} {@code message}
     * correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected >=\\[2015-05-02T15:15:15\\] but found \\[2015-05-01T15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_localDateTime_testFailNullMessage() {
        LocalDateTime actual = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        LocalDateTime expected = LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15);
        DateTimeAssert.assertNotBefore(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDateTime} that is equal to the
     * {@code expected} {@link LocalDateTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotBefore_localDateTime_testPassEqual() {
        LocalDateTime actual = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        DateTimeAssert.assertNotBefore(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalDateTime} that is not before the
     * {@code expected} {@link LocalDateTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotBefore_localDateTime_testPass() {
        LocalDateTime actual = LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15);
        LocalDateTime expected = LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15);
        DateTimeAssert.assertNotBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link LocalTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[null\\] but found \\[15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_localTime_nullExpected() {
        LocalTime actual = LocalTime.of(15, 15, 15);
        DateTimeAssert.assertNotBefore(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link LocalTime} correctly throws
     * an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[15:15:15\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_localTime_nullActual() {
        LocalTime expected = LocalTime.of(15, 15, 15);
        DateTimeAssert.assertNotBefore(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link LocalTime} and
     * {@code null} {@code actual} {@link LocalTime} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_localTime_nullExpectedAndActual() {
        DateTimeAssert.assertNotBefore(
            (LocalTime) null,
            (LocalTime) null,
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalTime} that is before the
     * {@code expected} {@link LocalTime} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[16:15:15\\] but found \\[15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_localTime_testFail() {
        LocalTime actual = LocalTime.of(15, 15, 15);
        LocalTime expected = LocalTime.of(16, 15, 15);
        DateTimeAssert.assertNotBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalTime} that is before the
     * {@code expected} {@link LocalTime} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected >=\\[16:15:15\\] but found \\[15:15:15\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_localTime_testFailNullMessage() {
        LocalTime actual = LocalTime.of(15, 15, 15);
        LocalTime expected = LocalTime.of(16, 15, 15);
        DateTimeAssert.assertNotBefore(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link LocalTime} that is equal to the
     * {@code expected} {@link LocalTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotBefore_localTime_testPassEqual() {
        LocalTime actual = LocalTime.of(15, 15, 15);
        DateTimeAssert.assertNotBefore(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link LocalTime} that is not before the
     * {@code expected} {@link LocalTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotBefore_localTime_testPass() {
        LocalTime actual = LocalTime.of(16, 15, 15);
        LocalTime expected = LocalTime.of(15, 15, 15);
        DateTimeAssert.assertNotBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link OffsetDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[null\\] but found \\[2015-05-01T15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_offsetDateTime_nullExpected() {
        OffsetDateTime actual = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotBefore(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link OffsetDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[2015-05-01T15:15:15-05:00\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_offsetDateTime_nullActual() {
        OffsetDateTime expected = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotBefore(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link OffsetDateTime} and
     * {@code null} {@code actual} {@link OffsetDateTime} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_offsetDateTime_nullExpectedAndActual() {
        DateTimeAssert.assertNotBefore(
            (OffsetDateTime) null,
            (OffsetDateTime) null,
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetDateTime} that is before the
     * {@code expected} {@link OffsetDateTime} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[2015-05-02T15:15:15-05:00\\] but found \\[2015-05-01T15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_offsetDateTime_testFail() {
        OffsetDateTime actual = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetDateTime expected = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetDateTime} that is before the
     * {@code expected} {@link OffsetDateTime} with a {@code null} {@code message}
     * correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected >=\\[2015-05-02T15:15:15-05:00\\] but found \\[2015-05-01T15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_offsetDateTime_testFailNullMessage() {
        OffsetDateTime actual = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetDateTime expected = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotBefore(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetDateTime} that is equal to the
     * {@code expected} {@link OffsetDateTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotBefore_offsetDateTime_testPassEqual() {
        OffsetDateTime actual = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotBefore(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetDateTime} that is not before the
     * {@code expected} {@link OffsetDateTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotBefore_offsetDateTime_testPass() {
        OffsetDateTime actual = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetDateTime expected = OffsetDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link OffsetTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[null\\] but found \\[15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_offsetTime_nullExpected() {
        OffsetTime actual = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotBefore(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link OffsetTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[15:15:15-05:00\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_offsetTime_nullActual() {
        OffsetTime expected = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotBefore(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link OffsetTime} and
     * {@code null} {@code actual} {@link OffsetTime} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_offsetTime_nullExpectedAndActual() {
        DateTimeAssert.assertNotBefore(
            (OffsetTime) null,
            (OffsetTime) null,
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetTime} that is before the
     * {@code expected} {@link OffsetTime} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[16:15:15-05:00\\] but found \\[15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_offsetTime_testFail() {
        OffsetTime actual = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetTime expected = OffsetTime.of(
            LocalTime.of(16, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetTime} that is before the
     * {@code expected} {@link OffsetTime} with a {@code null} {@code message} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected >=\\[16:15:15-05:00\\] but found \\[15:15:15-05:00\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_offsetTime_testFailNullMessage() {
        OffsetTime actual = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetTime expected = OffsetTime.of(
            LocalTime.of(16, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotBefore(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetTime} that is equal to the
     * {@code expected} {@link OffsetTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotBefore_offsetTime_testPassEqual() {
        OffsetTime actual = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotBefore(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link OffsetTime} that is not before the
     * {@code expected} {@link OffsetTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotBefore_offsetTime_testPass() {
        OffsetTime actual = OffsetTime.of(
            LocalTime.of(16, 15, 15),
            ZoneOffset.ofHours(-5));
        OffsetTime expected = OffsetTime.of(
            LocalTime.of(15, 15, 15),
            ZoneOffset.ofHours(-5));
        DateTimeAssert.assertNotBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code expected} {@link ZonedDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[null\\] but found \\[2015-05-01T15:15:15-05:00\\[America/Winnipeg\\]\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_zonedDateTime_nullExpected() {
        ZonedDateTime actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertNotBefore(actual, null, "test message");
    }

    /**
     * Tests that passing a {@code null} {@code actual} {@link ZonedDateTime} correctly
     * throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[2015-05-01T15:15:15-05:00\\[America/Winnipeg\\]\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_zonedDateTime_nullActual() {
        ZonedDateTime expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertNotBefore(null, expected, "test message");
    }

    /**
     * Tests that passing both a {@code null} {@code expected} {@link ZonedDateTime} and
     * {@code null} {@code actual} {@link ZonedDateTime} correctly throws an
     * {@link AssertionError} .
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[null\\] but found \\[null\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_zonedDateTime_nullExpectedAndActual() {
        DateTimeAssert.assertNotBefore(
            (ZonedDateTime) null,
            (ZonedDateTime) null,
            "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link ZonedDateTime} that is before the
     * {@code expected} {@link ZonedDateTime} correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "test message: expected >=\\[2015-05-02T15:15:15-05:00\\[America/Winnipeg\\]\\] but found \\[2015-05-01T15:15:15-05:00\\[America/Winnipeg\\]\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_zonedDateTime_testFail() {
        ZonedDateTime actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        ZonedDateTime expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertNotBefore(actual, expected, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link ZonedDateTime} that is before the
     * {@code expected} {@link ZonedDateTime} with a {@code null} {@code message}
     * correctly throws an {@link AssertionError}.
     */
    @Test(
        expectedExceptions = AssertionError.class,
        expectedExceptionsMessageRegExp = "null: expected >=\\[2015-05-02T15:15:15-05:00\\[America/Winnipeg\\]\\] but found \\[2015-05-01T15:15:15-05:00\\[America/Winnipeg\\]\\]",
        groups = { "unit-tests" })
    public static void testAssertNotBefore_zonedDateTime_testFailNullMessage() {
        ZonedDateTime actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        ZonedDateTime expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertNotBefore(actual, expected, null);
    }

    /**
     * Tests that passing an {@code actual} {@link ZonedDateTime} that is equal to the
     * {@code expected} {@link ZonedDateTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotBefore_zonedDateTime_testPassEqual() {
        ZonedDateTime actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertNotBefore(actual, actual, "test message");
    }

    /**
     * Tests that passing an {@code actual} {@link ZonedDateTime} that is not before the
     * {@code expected} {@link ZonedDateTime} correctly does not throw an
     * {@link AssertionError}.
     */
    @Test(groups = { "unit-tests" })
    public static void testAssertNotBefore_zonedDateTime_testPass() {
        ZonedDateTime actual = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 2, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        ZonedDateTime expected = ZonedDateTime.of(
            LocalDateTime.of(2015, Month.MAY, 1, 15, 15, 15),
            ZoneId.of("America/Winnipeg"));
        DateTimeAssert.assertNotBefore(actual, expected, "test message");
    }
}
