/*
 * Copyright 2016 Matthew Ammeter
 *
 * This file is part of test-utilities.
 *
 * test-utilities is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * test-utilities is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with test-utilities.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.ammeter.test.utilities.asserts;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;

/**
 * Testing asserts for working with dates, times, and date/times.
 *
 * @author Matthew Ammeter
 */
public class DateTimeAssert {
    /**
     * Asserts that the {@code actual} {@link Calendar} occurs after the {@code expected}
     * {@link Calendar}. If it does not, an {@link AssertionError}, with the given
     * message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertAfter(Calendar actual, Calendar expected, String message) {
        if (actual == null || expected == null || !actual.after(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatAfter(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link Date} occurs after the {@code expected}
     * {@link Date}. If it does not, an {@link AssertionError}, with the given message, is
     * thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertAfter(Date actual, Date expected, String message) {
        if (actual == null || expected == null || !actual.after(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatAfter(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link Instant} occurs after the {@code expected}
     * {@link Instant}. If it does not, an {@link AssertionError}, with the given message,
     * is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertAfter(Instant actual, Instant expected, String message) {
        if (actual == null || expected == null || !actual.isAfter(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatAfter(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link LocalDate} occurs after the {@code expected}
     * {@link LocalDate}. If it does not, an {@link AssertionError}, with the given
     * message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertAfter(LocalDate actual, LocalDate expected, String message) {
        if (actual == null || expected == null || !actual.isAfter(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatAfter(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link LocalDateTime} occurs after the
     * {@code expected} {@link LocalDateTime}. If it does not, an {@link AssertionError},
     * with the given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertAfter(
            LocalDateTime actual,
            LocalDateTime expected,
            String message) {
        if (actual == null || expected == null || !actual.isAfter(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatAfter(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link LocalTime} occurs after the {@code expected}
     * {@link LocalTime}. If it does not, an {@link AssertionError}, with the given
     * message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertAfter(LocalTime actual, LocalTime expected, String message) {
        if (actual == null || expected == null || !actual.isAfter(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatAfter(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link OffsetDateTime} occurs after the
     * {@code expected} {@link OffsetDateTime}. If it does not, an {@link AssertionError}
     * , with the given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertAfter(
            OffsetDateTime actual,
            OffsetDateTime expected,
            String message) {
        if (actual == null || expected == null || !actual.isAfter(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatAfter(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link OffsetTime} occurs after the
     * {@code expected} {@link OffsetTime}. If it does not, an {@link AssertionError},
     * with the given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertAfter(
            OffsetTime actual,
            OffsetTime expected,
            String message) {
        if (actual == null || expected == null || !actual.isAfter(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatAfter(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link ZonedDateTime} occurs after the
     * {@code expected} {@link ZonedDateTime}. If it does not, an {@link AssertionError},
     * with the given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertAfter(
            ZonedDateTime actual,
            ZonedDateTime expected,
            String message) {
        if (actual == null || expected == null || !actual.isAfter(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatAfter(actual, expected, message));
        }
    }

    private static String formatAfter(
            Calendar actual,
            Calendar expected,
            String message) {
        return String.format(
            "%s: expected >[%tc] but found [%tc]",
            message,
            expected,
            actual);
    }

    private static String formatAfter(Object actual, Object expected, String message) {
        return String.format(
            "%s: expected >[%s] but found [%s]",
            message,
            expected,
            actual);
    }

    /**
     * Asserts that the {@code actual} {@link Calendar} occurs before the {@code expected}
     * {@link Calendar}. If it does not, an {@link AssertionError}, with the given
     * message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertBefore(Calendar actual, Calendar expected, String message) {
        if (actual == null || expected == null || !actual.before(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatBefore(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link Date} occurs before the {@code expected}
     * {@link Date}. If it does not, an {@link AssertionError}, with the given message, is
     * thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertBefore(Date actual, Date expected, String message) {
        if (actual == null || expected == null || !actual.before(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatBefore(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link Instant} occurs before the {@code expected}
     * {@link Instant}. If it does not, an {@link AssertionError}, with the given message,
     * is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertBefore(Instant actual, Instant expected, String message) {
        if (actual == null || expected == null || !actual.isBefore(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatBefore(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link LocalDate} occurs before the
     * {@code expected} {@link LocalDate}. If it does not, an {@link AssertionError}, with
     * the given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertBefore(
            LocalDate actual,
            LocalDate expected,
            String message) {
        if (actual == null || expected == null || !actual.isBefore(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatBefore(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link LocalDateTime} occurs before the
     * {@code expected} {@link LocalDateTime}. If it does not, an {@link AssertionError},
     * with the given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertBefore(
            LocalDateTime actual,
            LocalDateTime expected,
            String message) {
        if (actual == null || expected == null || !actual.isBefore(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatBefore(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link LocalTime} occurs before the
     * {@code expected} {@link LocalTime}. If it does not, an {@link AssertionError}, with
     * the given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertBefore(
            LocalTime actual,
            LocalTime expected,
            String message) {
        if (actual == null || expected == null || !actual.isBefore(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatBefore(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link OffsetDateTime} occurs before the
     * {@code expected} {@link OffsetDateTime}. If it does not, an {@link AssertionError}
     * , with the given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertBefore(
            OffsetDateTime actual,
            OffsetDateTime expected,
            String message) {
        if (actual == null || expected == null || !actual.isBefore(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatBefore(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link OffsetTime} occurs before the
     * {@code expected} {@link OffsetTime}. If it does not, an {@link AssertionError},
     * with the given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertBefore(
            OffsetTime actual,
            OffsetTime expected,
            String message) {
        if (actual == null || expected == null || !actual.isBefore(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatBefore(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link ZonedDateTime} occurs before the
     * {@code expected} {@link ZonedDateTime}. If it does not, an {@link AssertionError},
     * with the given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertBefore(
            ZonedDateTime actual,
            ZonedDateTime expected,
            String message) {
        if (actual == null || expected == null || !actual.isBefore(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatBefore(actual, expected, message));
        }
    }

    private static String formatBefore(
            Calendar actual,
            Calendar expected,
            String message) {
        return String.format(
            "%s: expected <[%tc] but found [%tc]",
            message,
            expected,
            actual);
    }

    private static String formatBefore(Object actual, Object expected, String message) {
        return String.format(
            "%s: expected <[%s] but found [%s]",
            message,
            expected,
            actual);
    }

    /**
     * Asserts that the {@code actual} {@link Calendar} does not occur after the
     * {@code expected} {@link Calendar}. If it does, an {@link AssertionError}, with the
     * given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertNotAfter(
            Calendar actual,
            Calendar expected,
            String message) {
        if (actual == null || expected == null || actual.after(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatNotAfter(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link Date} does not occur after the
     * {@code expected} {@link Date}. If it does, an {@link AssertionError}, with the
     * given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertNotAfter(Date actual, Date expected, String message) {
        if (actual == null || expected == null || actual.after(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatNotAfter(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link Instant} does not occur after the
     * {@code expected} {@link Instant}. If it does, an {@link AssertionError}, with the
     * given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertNotAfter(Instant actual, Instant expected, String message) {
        if (actual == null || expected == null || actual.isAfter(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatNotAfter(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link LocalDate} does not occur after the
     * {@code expected} {@link LocalDate}. If it does, an {@link AssertionError}, with the
     * given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertNotAfter(
            LocalDate actual,
            LocalDate expected,
            String message) {
        if (actual == null || expected == null || actual.isAfter(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatNotAfter(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link LocalDateTime} does not occur after the
     * {@code expected} {@link LocalDateTime}. If it does, an {@link AssertionError}, with
     * the given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertNotAfter(
            LocalDateTime actual,
            LocalDateTime expected,
            String message) {
        if (actual == null || expected == null || actual.isAfter(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatNotAfter(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link LocalTime} does not occur after the
     * {@code expected} {@link LocalTime}. If it does, an {@link AssertionError}, with the
     * given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertNotAfter(
            LocalTime actual,
            LocalTime expected,
            String message) {
        if (actual == null || expected == null || actual.isAfter(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatNotAfter(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link OffsetDateTime} does not occur after the
     * {@code expected} {@link OffsetDateTime}. If it does, an {@link AssertionError} ,
     * with the given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertNotAfter(
            OffsetDateTime actual,
            OffsetDateTime expected,
            String message) {
        if (actual == null || expected == null || actual.isAfter(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatNotAfter(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link OffsetTime} does not occur after the
     * {@code expected} {@link OffsetTime}. If it does, an {@link AssertionError}, with
     * the given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertNotAfter(
            OffsetTime actual,
            OffsetTime expected,
            String message) {
        if (actual == null || expected == null || actual.isAfter(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatNotAfter(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link ZonedDateTime} does not occur after the
     * {@code expected} {@link ZonedDateTime}. If it does, an {@link AssertionError}, with
     * the given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertNotAfter(
            ZonedDateTime actual,
            ZonedDateTime expected,
            String message) {
        if (actual == null || expected == null || actual.isAfter(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatNotAfter(actual, expected, message));
        }
    }

    private static String formatNotAfter(
            Calendar actual,
            Calendar expected,
            String message) {
        return String.format(
            "%s: expected <=[%tc] but found [%tc]",
            message,
            expected,
            actual);
    }

    private static String formatNotAfter(Object actual, Object expected, String message) {
        return String.format(
            "%s: expected <=[%s] but found [%s]",
            message,
            expected,
            actual);
    }

    /**
     * Asserts that the {@code actual} {@link Calendar} does not occur before the
     * {@code expected} {@link Calendar}. If it does, an {@link AssertionError}, with the
     * given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertNotBefore(
            Calendar actual,
            Calendar expected,
            String message) {
        if (actual == null || expected == null || actual.before(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatNotBefore(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link Date} does not occur before the
     * {@code expected} {@link Date}. If it does, an {@link AssertionError}, with the
     * given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertNotBefore(Date actual, Date expected, String message) {
        if (actual == null || expected == null || actual.before(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatNotBefore(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link Instant} does not occur before the
     * {@code expected} {@link Instant}. If it does, an {@link AssertionError}, with the
     * given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertNotBefore(Instant actual, Instant expected, String message) {
        if (actual == null || expected == null || actual.isBefore(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatNotBefore(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link LocalDate} does not occur before the
     * {@code expected} {@link LocalDate}. If it does, an {@link AssertionError}, with the
     * given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertNotBefore(
            LocalDate actual,
            LocalDate expected,
            String message) {
        if (actual == null || expected == null || actual.isBefore(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatNotBefore(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link LocalDateTime} does not occur before the
     * {@code expected} {@link LocalDateTime}. If it does, an {@link AssertionError}, with
     * the given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertNotBefore(
            LocalDateTime actual,
            LocalDateTime expected,
            String message) {
        if (actual == null || expected == null || actual.isBefore(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatNotBefore(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link LocalTime} does not occur before the
     * {@code expected} {@link LocalTime}. If it does, an {@link AssertionError}, with the
     * given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertNotBefore(
            LocalTime actual,
            LocalTime expected,
            String message) {
        if (actual == null || expected == null || actual.isBefore(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatNotBefore(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link OffsetDateTime} does not occur before the
     * {@code expected} {@link OffsetDateTime}. If it does, an {@link AssertionError} ,
     * with the given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertNotBefore(
            OffsetDateTime actual,
            OffsetDateTime expected,
            String message) {
        if (actual == null || expected == null || actual.isBefore(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatNotBefore(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link OffsetTime} does not occur before the
     * {@code expected} {@link OffsetTime}. If it does, an {@link AssertionError}, with
     * the given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertNotBefore(
            OffsetTime actual,
            OffsetTime expected,
            String message) {
        if (actual == null || expected == null || actual.isBefore(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatNotBefore(actual, expected, message));
        }
    }

    /**
     * Asserts that the {@code actual} {@link ZonedDateTime} does not occur before the
     * {@code expected} {@link ZonedDateTime}. If it does, an {@link AssertionError}, with
     * the given message, is thrown.
     *
     * @param actual The actual value being compared.
     * @param expected the expected value.
     * @param message The assertion error message.
     */
    public static void assertNotBefore(
            ZonedDateTime actual,
            ZonedDateTime expected,
            String message) {
        if (actual == null || expected == null || actual.isBefore(expected)) {
            throw new AssertionError(
                DateTimeAssert.formatNotBefore(actual, expected, message));
        }
    }

    private static String formatNotBefore(
            Calendar actual,
            Calendar expected,
            String message) {
        return String.format(
            "%s: expected >=[%tc] but found [%tc]",
            message,
            expected,
            actual);
    }

    private static String formatNotBefore(
            Object actual,
            Object expected,
            String message) {
        return String.format(
            "%s: expected >=[%s] but found [%s]",
            message,
            expected,
            actual);
    }
}
