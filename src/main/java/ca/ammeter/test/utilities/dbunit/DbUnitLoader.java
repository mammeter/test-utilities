/*
 * Copyright 2016 Matthew Ammeter
 *
 * This file is part of test-utilities.
 *
 * test-utilities is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * test-utilities is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * test-utilities. If not, see <http://www.gnu.org/licenses/>.
 */
package ca.ammeter.test.utilities.dbunit;

import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

import javax.sql.DataSource;

import com.google.common.base.Preconditions;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.io.Resources;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.DataSourceUtils;

/**
 * Fluent-style utility for pre-loading test data in to databases.
 * <p>
 * Instances of this class can be created by calling either
 * {@link DbUnitLoader#createSingleDataSourceInstance(DataSource)} if the test only
 * requires a single {@link DataSource}, or by calling
 * {@link DbUnitLoader#createMultiDataSourceInstance()} if multiple {@link DataSource}s
 * are required. If {@link DbUnitLoader#createMultiDataSourceInstance()} is used, every
 * call to add data sets or properties must specify the {@link DataSource} those objects
 * are for.
 *
 * @author Matthew Ammeter
 */
public final class DbUnitLoader {
    private static final Logger logger = LoggerFactory.getLogger(DbUnitLoader.class);

    private final ListMultimap<DataSource, SchemaDataSet> dataSets;

    private final Map<DataSource, Properties> dbUnitProperties;

    private final Map<String, Object> replacementData;

    private final DataSource singleDataSource;

    /**
     * Returns a new {@link DbUnitLoader} for loading data in to multiple
     * {@link DataSource}s.
     *
     * @return A multi-data source instance of {@link DbUnitLoader}.
     */
    public static DbUnitLoader createMultiDataSourceInstance() {
        DbUnitLoader.logger.debug("Creating new multi-data source instance");

        return new DbUnitLoader(null);
    }

    /**
     * Returns a new {@link DbUnitLoader} for loading data in to a single
     * {@link DataSource}s.
     *
     * @param dataSource The data source to load.
     *
     * @return A single data source instance of {@link DbUnitLoader}.
     */
    public static DbUnitLoader createSingleDataSourceInstance(DataSource dataSource) {
        Preconditions.checkNotNull(dataSource, "The data source cannot be null");

        DbUnitLoader.logger.debug("Creating new single data source instance");

        return new DbUnitLoader(dataSource);
    }

    private DbUnitLoader(DataSource singleDataSource) {
        DbUnitLoader.logger.debug("Initializing class");

        // LinkedListMultimap guarantees key ordering as well as value ordering,
        // so schemas will be inserted in order.
        this.dataSets = LinkedListMultimap.create();
        this.dbUnitProperties = new HashMap<>();
        this.replacementData = new HashMap<>();

        this.singleDataSource = singleDataSource;
    }

    private boolean isSingleDataSource() {
        return this.singleDataSource != null;
    }

    private boolean isDataSourceAllowed(DataSource dataSource) {
        return !this.isSingleDataSource() || Objects.equals(
            this.singleDataSource,
            dataSource);
    }

    /**
     * Adds the data set located at the given file path for the given schema to the list
     * of data sets to be loaded. Note that this method is only valid for a single-data
     * source instance of {@link DbUnitLoader}.
     *
     * @param schema The schema the data set will be loaded in to.
     * @param filePath The absolute path to the data set within the classloader.
     *
     * @throws IllegalArgumentException If {@code filePath} does not exist.
     * @throws IllegalStateException If this is not a single data source instance.
     * @throws NullPointerException If any of {@code schema} or {@code filePath} are
     *         {@code null}.
     *
     * @return This instance.
     *
     * @see DbUnitLoader#createSingleDataSourceInstance(DataSource)
     */
    public DbUnitLoader addDataSet(String schema, String filePath) {
        Preconditions.checkNotNull(schema, "The schema cannot be null");
        Preconditions.checkNotNull(filePath, "The file path cannot be null");

        Preconditions.checkState(
            this.isSingleDataSource(),
            "Must specify a data source in multi-datasource instance");

        DbUnitLoader.logger.debug(
            "Adding data set {} for schema {}, using database operation {}",
            filePath,
            schema,
            DatabaseOperation.REFRESH);

        return this.addDataSet(
            this.singleDataSource,
            DatabaseOperation.REFRESH,
            schema,
            filePath);
    }

    /**
     * Adds the data set located at the given file path for the given schema to the list
     * of data sets to be loaded}. Note that this method is only valid for a single-data
     * source instance of {@link DbUnitLoader}.
     *
     * @param schema The schema the data set will be loaded in to.
     * @param filePath The path to the data set within the classloader, relative to
     *        {@code relativeClass}.
     * @param relativeClass The class {@code filePath} is relative to.
     *
     * @throws IllegalArgumentException If {@code filePath} does not exist.
     * @throws IllegalStateException If this is not a single data source instance.
     * @throws NullPointerException If any of {@code schema}, {@code filePath}, or
     *         {@code relativeClass} are {@code null}.
     *
     * @return This instance.
     *
     * @see DbUnitLoader#createSingleDataSourceInstance(DataSource)
     */
    public DbUnitLoader addDataSet(
            String schema,
            String filePath,
            Class<?> relativeClass) {
        Preconditions.checkNotNull(schema, "The schema cannot be null");
        Preconditions.checkNotNull(filePath, "The file path cannot be null");
        Preconditions.checkNotNull(relativeClass, "The relative class cannot be null");

        Preconditions.checkState(
            this.isSingleDataSource(),
            "Must specify a data source in multi-datasource instance");

        DbUnitLoader.logger.debug(
            "Adding data set {} relative to {} for schema {}, using database operation {}",
            filePath,
            relativeClass.getName(),
            schema,
            DatabaseOperation.REFRESH);

        return this.addDataSet(
            this.singleDataSource,
            DatabaseOperation.REFRESH,
            schema,
            filePath,
            relativeClass);
    }

    /**
     * Adds the data set located at the given {@link URL} for the given schema to the list
     * of data sets to be loaded. Note that this method is only valid for a single-data
     * source instance of {@link DbUnitLoader}.
     *
     * @param schema The schema the data set will be loaded in to.
     * @param url The URL of the data set.
     *
     * @throws IllegalStateException If this is not a single data source instance.
     * @throws NullPointerException If any of {@code schema} or {@code url} are
     *         {@code null}.
     *
     * @return This instance.
     *
     * @see DbUnitLoader#createSingleDataSourceInstance(DataSource)
     */
    public DbUnitLoader addDataSet(String schema, URL url) {
        Preconditions.checkNotNull(schema, "The schema cannot be null");
        Preconditions.checkNotNull(url, "The URL cannot be null");

        Preconditions.checkState(
            this.isSingleDataSource(),
            "Must specify a data source in multi-datasource instance");

        DbUnitLoader.logger.debug(
            "Adding data set {} for schema {}, using database operation {}",
            url,
            schema,
            DatabaseOperation.REFRESH);

        return this.addDataSet(
            this.singleDataSource,
            DatabaseOperation.REFRESH,
            schema,
            url);
    }

    /**
     * Adds the data set located at the given file path for the given schema to the list
     * of data sets to be loaded by the given {@link DatabaseOperation}. Note that this
     * method is only valid for a single-data source instance of {@link DbUnitLoader}.
     *
     * @param databaseOperation The method by which the data will be loaded.
     * @param schema The schema the data set will be loaded in to.
     * @param filePath The absolute path to the data set within the classloader.
     *
     * @throws IllegalArgumentException If {@code filePath} does not exist.
     * @throws IllegalStateException If this is not a single data source instance.
     * @throws NullPointerException If any of {@code databaseOperation}, {@code schema},
     *         or {@code filePath} are {@code null}.
     *
     * @return This instance.
     *
     * @see DbUnitLoader#createSingleDataSourceInstance(DataSource)
     */
    public DbUnitLoader addDataSet(
            DatabaseOperation databaseOperation,
            String schema,
            String filePath) {
        Preconditions.checkNotNull(
            databaseOperation,
            "The database operation cannot be null");
        Preconditions.checkNotNull(schema, "The schema cannot be null");
        Preconditions.checkNotNull(filePath, "The file path cannot be null");

        Preconditions.checkState(
            this.isSingleDataSource(),
            "Must specify a data source in multi-datasource instance");

        DbUnitLoader.logger.debug(
            "Adding data set {} for schema {}, using database operation {}",
            filePath,
            schema,
            databaseOperation);

        return this.addDataSet(
            this.singleDataSource,
            databaseOperation,
            schema,
            filePath);
    }

    /**
     * Adds the data set located at the given file path for the given schema to the list
     * of data sets to be loaded by the given {@link DatabaseOperation}. Note that this
     * method is only valid for a single-data source instance of {@link DbUnitLoader}.
     *
     * @param databaseOperation The method by which the data will be loaded.
     * @param schema The schema the data set will be loaded in to.
     * @param filePath The path to the data set within the classloader, relative to
     *        {@code relativeClass}.
     * @param relativeClass The class {@code filePath} is relative to.
     *
     * @throws IllegalArgumentException If {@code filePath} does not exist.
     * @throws IllegalStateException If this is not a single data source instance.
     * @throws NullPointerException If any of {@code databaseOperation}, {@code schema},
     *         {@code filePath}, or {@code relativeClass} are {@code null}.
     *
     * @return This instance.
     *
     * @see DbUnitLoader#createSingleDataSourceInstance(DataSource)
     */
    public DbUnitLoader addDataSet(
            DatabaseOperation databaseOperation,
            String schema,
            String filePath,
            Class<?> relativeClass) {
        Preconditions.checkNotNull(
            databaseOperation,
            "The database operation cannot be null");
        Preconditions.checkNotNull(schema, "The schema cannot be null");
        Preconditions.checkNotNull(filePath, "The file path cannot be null");
        Preconditions.checkNotNull(relativeClass, "The relative class cannot be null");

        Preconditions.checkState(
            this.isSingleDataSource(),
            "Must specify a data source in multi-datasource instance");

        DbUnitLoader.logger.debug(
            "Adding data set {} relative to {} for schema {}, using database operation {}",
            filePath,
            relativeClass.getName(),
            schema,
            databaseOperation);

        return this.addDataSet(
            this.singleDataSource,
            databaseOperation,
            schema,
            filePath,
            relativeClass);
    }

    /**
     * Adds the data set located at the given {@link URL} for the given schema to the list
     * of data sets to be loaded by the given {@link DatabaseOperation}. Note that this
     * method is only valid for a single-data source instance of {@link DbUnitLoader}.
     *
     * @param databaseOperation The method by which the data will be loaded.
     * @param schema The schema the data set will be loaded in to.
     * @param url The URL of the data set.
     *
     * @throws IllegalStateException If this is not a single data source instance.
     * @throws NullPointerException If any of {@code databaseOperation}, {@code schema},
     *         or {@code url} are {@code null}.
     *
     * @return This instance.
     *
     * @see DbUnitLoader#createSingleDataSourceInstance(DataSource)
     */
    public DbUnitLoader addDataSet(
            DatabaseOperation databaseOperation,
            String schema,
            URL url) {
        Preconditions.checkNotNull(
            databaseOperation,
            "The database operation cannot be null");
        Preconditions.checkNotNull(schema, "The schema cannot be null");
        Preconditions.checkNotNull(url, "The URL cannot be null");

        Preconditions.checkState(
            this.isSingleDataSource(),
            "Must specify a data source in multi-datasource instance");

        DbUnitLoader.logger.debug(
            "Adding data set {} for schema {}, using database operation {}",
            url,
            schema,
            databaseOperation);

        return this.addDataSet(this.singleDataSource, databaseOperation, schema, url);
    }

    /**
     * Adds the data set located at the given file path for the given schema to the list
     * of data sets to be loaded for the given {@link DataSource}.
     *
     * @param dataSource The data source the data set will be loaded in to.
     * @param schema The schema the data set will be loaded in to.
     * @param filePath The absolute path to the data set within the classloader.
     *
     * @throws IllegalArgumentException If {@code filePath} does not exist.
     * @throws IllegalStateException If this is a single data source instance and
     *         {@code dataSource} is not the data source used to create this instance.
     * @throws NullPointerException If any of {@code dataSource}, {@code schema} , or
     *         {@code filePath} are {@code null}.
     *
     * @return This instance.
     */
    public DbUnitLoader addDataSet(
            DataSource dataSource,
            String schema,
            String filePath) {
        Preconditions.checkNotNull(dataSource, "The data source cannot be null");
        Preconditions.checkNotNull(schema, "The schema cannot be null");
        Preconditions.checkNotNull(filePath, "The file path cannot be null");

        Preconditions.checkState(
            this.isDataSourceAllowed(dataSource),
            "Cannot add second datasource to single-datasource instance");

        DbUnitLoader.logger.debug(
            "Adding data set {} for schema {}, using database operation {}",
            filePath,
            schema,
            DatabaseOperation.REFRESH);

        return this.addDataSet(dataSource, DatabaseOperation.REFRESH, schema, filePath);
    }

    /**
     * Adds the data set located at the given file path for the given schema to the list
     * of data sets to be loaded for the given {@link DataSource}.
     *
     * @param dataSource The data source the data set will be loaded in to.
     * @param schema The schema the data set will be loaded in to.
     * @param filePath The path to the data set within the classloader, relative to
     *        {@code relativeClass}.
     * @param relativeClass The class {@code filePath} is relative to.
     *
     * @throws IllegalArgumentException If {@code filePath} does not exist.
     * @throws IllegalStateException If this is a single data source instance and
     *         {@code dataSource} is not the data source used to create this instance.
     * @throws NullPointerException If any of {@code dataSource}, {@code schema} ,
     *         {@code filePath}, or {@code relativeClass} are {@code null}.
     *
     * @return This instance.
     */
    public DbUnitLoader addDataSet(
            DataSource dataSource,
            String schema,
            String filePath,
            Class<?> relativeClass) {
        Preconditions.checkNotNull(dataSource, "The data source cannot be null");
        Preconditions.checkNotNull(schema, "The schema cannot be null");
        Preconditions.checkNotNull(filePath, "The file path cannot be null");
        Preconditions.checkNotNull(relativeClass, "The relative class cannot be null");

        Preconditions.checkState(
            this.isDataSourceAllowed(dataSource),
            "Cannot add second datasource to single-datasource instance");

        DbUnitLoader.logger.debug(
            "Adding data set {} relative to {} for schema {}, using database operation {}",
            filePath,
            relativeClass.getName(),
            schema,
            DatabaseOperation.REFRESH);

        return this.addDataSet(
            dataSource,
            DatabaseOperation.REFRESH,
            schema,
            filePath,
            relativeClass);
    }

    /**
     * Adds the data set located at the given {@link URL} for the given schema to the list
     * of data sets to be loaded for the given {@link DataSource}.
     *
     * @param dataSource The data source the data set will be loaded in to.
     * @param schema The schema the data set will be loaded in to.
     * @param url The URL of the data set.
     *
     * @throws IllegalStateException If this is a single data source instance and
     *         {@code dataSource} is not the data source used to create this instance.
     * @throws NullPointerException If any of {@code dataSource}, {@code schema} , or
     *         {@code url} are {@code null}.
     *
     * @return This instance.
     */
    public DbUnitLoader addDataSet(DataSource dataSource, String schema, URL url) {
        Preconditions.checkNotNull(dataSource, "The data source cannot be null");
        Preconditions.checkNotNull(schema, "The schema cannot be null");
        Preconditions.checkNotNull(url, "The URL cannot be null");

        Preconditions.checkState(
            this.isDataSourceAllowed(dataSource),
            "Cannot add second datasource to single-datasource instance");

        DbUnitLoader.logger.debug(
            "Adding data set {} for schema {}, using database operation {}",
            url,
            schema,
            DatabaseOperation.REFRESH);

        return this.addDataSet(dataSource, DatabaseOperation.REFRESH, schema, url);
    }

    /**
     * Adds the data set located at the given file path for the given schema to the list
     * of data sets to be loaded by the given {@link DatabaseOperation}, for the given
     * {@link DataSource}.
     *
     * @param dataSource The data source the data set will be loaded in to.
     * @param databaseOperation The method by which the data will be loaded.
     * @param schema The schema the data set will be loaded in to.
     * @param filePath The absolute path to the data set within the classloader.
     *
     * @throws IllegalArgumentException If {@code filePath} does not exist.
     * @throws IllegalStateException If this is a single data source instance and
     *         {@code dataSource} is not the data source used to create this instance.
     * @throws NullPointerException If any of {@code dataSource},
     *         {@code databaseOperation}, {@code schema}, or {@code filePath} are
     *         {@code null}.
     *
     * @return This instance.
     */
    public DbUnitLoader addDataSet(
            DataSource dataSource,
            DatabaseOperation databaseOperation,
            String schema,
            String filePath) {
        Preconditions.checkNotNull(dataSource, "The data source cannot be null");
        Preconditions.checkNotNull(
            databaseOperation,
            "The database operation cannot be null");
        Preconditions.checkNotNull(schema, "The schema cannot be null");
        Preconditions.checkNotNull(filePath, "The file path cannot be null");

        Preconditions.checkState(
            this.isDataSourceAllowed(dataSource),
            "Cannot add second datasource to single-datasource instance");

        DbUnitLoader.logger.debug(
            "Adding data set {} for schema {}, using database operation {}",
            filePath,
            schema,
            databaseOperation);

        URL url = Resources.getResource(filePath);

        return this.addDataSet(dataSource, databaseOperation, schema, url);
    }

    /**
     * Adds the data set located at the given file path for the given schema to the list
     * of data sets to be loaded by the given {@link DatabaseOperation}, for the given
     * {@link DataSource}.
     *
     * @param dataSource The data source the data set will be loaded in to.
     * @param databaseOperation The method by which the data will be loaded.
     * @param schema The schema the data set will be loaded in to.
     * @param filePath The path to the data set within the classloader, relative to
     *        {@code relativeClass}.
     * @param relativeClass The class {@code filePath} is relative to.
     *
     * @throws IllegalArgumentException If {@code filePath} does not exist.
     * @throws IllegalStateException If this is a single data source instance and
     *         {@code dataSource} is not the data source used to create this instance.
     * @throws NullPointerException If any of {@code dataSource},
     *         {@code databaseOperation}, {@code schema} , {@code filePath}, or
     *         {@code relativeClass} are {@code null}.
     *
     * @return This instance.
     */
    public DbUnitLoader addDataSet(
            DataSource dataSource,
            DatabaseOperation databaseOperation,
            String schema,
            String filePath,
            Class<?> relativeClass) {
        Preconditions.checkNotNull(dataSource, "The data source cannot be null");
        Preconditions.checkNotNull(
            databaseOperation,
            "The database operation cannot be null");
        Preconditions.checkNotNull(schema, "The schema cannot be null");
        Preconditions.checkNotNull(filePath, "The file path cannot be null");
        Preconditions.checkNotNull(relativeClass, "The relative class cannot be null");

        Preconditions.checkState(
            this.isDataSourceAllowed(dataSource),
            "Cannot add second datasource to single-datasource instance");

        DbUnitLoader.logger.debug(
            "Adding data set {} relative to {} for schema {}, using database operation {}",
            filePath,
            relativeClass.getName(),
            schema,
            databaseOperation);

        URL url = Resources.getResource(relativeClass, filePath);

        return this.addDataSet(dataSource, databaseOperation, schema, url);
    }

    /**
     * Adds the data set located at the given {@link URL} for the given schema to the list
     * of data sets to be loaded by the given {@link DatabaseOperation}, for the given
     * {@link DataSource}.
     *
     * @param dataSource The data source the data set will be loaded in to.
     * @param databaseOperation The method by which the data will be loaded.
     * @param schema The schema the data set will be loaded in to.
     * @param url The URL of the data set.
     *
     * @throws IllegalStateException If this is a single data source instance and
     *         {@code dataSource} is not the data source used to create this instance.
     * @throws NullPointerException If any of {@code dataSource},
     *         {@code databaseOperation}, {@code schema}, or {@code url} are {@code null}.
     *
     * @return This instance.
     */
    public DbUnitLoader addDataSet(
            DataSource dataSource,
            DatabaseOperation databaseOperation,
            String schema,
            URL url) {
        Preconditions.checkNotNull(dataSource, "The data source cannot be null");
        Preconditions.checkNotNull(
            databaseOperation,
            "The database operation cannot be null");
        Preconditions.checkNotNull(schema, "The schema cannot be null");
        Preconditions.checkNotNull(url, "The URL cannot be null");

        Preconditions.checkState(
            this.isDataSourceAllowed(dataSource),
            "Cannot add second datasource to single-datasource instance");

        DbUnitLoader.logger.debug(
            "Adding data set {} for schema {}, using database operation {}",
            url,
            schema,
            DatabaseOperation.REFRESH);

        SchemaDataSet schemaDataSet = new SchemaDataSet(
            schema,
            DbUnitLoader.getDataSetFromFile(url),
            databaseOperation);
        this.dataSets.put(dataSource, schemaDataSet);

        return this;
    }

    private static IDataSet getDataSetFromFile(URL url) {
        DbUnitLoader.logger.debug("Building dataset from URL {}", url);

        IDataSet dataSet = null;
        try {
            dataSet = new FlatXmlDataSetBuilder().build(url);
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
        return dataSet;
    }

    /**
     * Adds the given {@code key}/{@code property} pair to the DB Unit properties
     * configured. All allowable keys are found in
     * {@link org.dbunit.database.DatabaseConfig}. Note that this method is only valid for
     * a single-data source instance of {@link DbUnitLoader}.
     *
     * @param key The key of the property.
     * @param property The property value.
     *
     * @return This instance.
     *
     * @throws IllegalStateException If this is not a single data source instance.
     * @throws NullPointerException If {@code key} is {@code null}.
     *
     * @see org.dbunit.database.DatabaseConfig
     */
    public DbUnitLoader addProperty(String key, String property) {
        Preconditions.checkNotNull(key, "The key cannot be null");

        Preconditions.checkState(
            this.isSingleDataSource(),
            "Must specify a data source in multi-datasource instance");

        DbUnitLoader.logger.debug("Adding property {}={}", key, property);

        return this.addProperty(this.singleDataSource, key, property);
    }

    /**
     * Adds the given {@code key}/{@code property} pair to the DB Unit properties
     * configured for the given {@link DataSource}. All allowable keys are found in
     * {@link org.dbunit.database.DatabaseConfig}.
     *
     * @param dataSource The data source the property is for.
     * @param key The key of the property.
     * @param property The property value.
     *
     * @return This instance.
     *
     * @throws IllegalStateException If this is a single data source instance and
     *         {@code dataSource} is not the data source used to create this instance.
     * @throws NullPointerException If {@code dataSource} or {@code key} are {@code null}.
     *
     * @see org.dbunit.database.DatabaseConfig
     */
    public DbUnitLoader addProperty(DataSource dataSource, String key, String property) {
        Preconditions.checkNotNull(dataSource, "The data source cannot be null");
        Preconditions.checkNotNull(key, "The key cannot be null");

        Preconditions.checkState(
            this.isDataSourceAllowed(dataSource),
            "Cannot add second datasource to single-datasource instance");

        DbUnitLoader.logger.debug("Adding property {}={}", key, property);

        Properties properties = this.getProperties(dataSource);
        properties.setProperty(key, property);

        return this;
    }

    private Properties getProperties(DataSource dataSource) {
        Properties properties = this.dbUnitProperties.get(dataSource);
        if (properties == null) {
            properties = new Properties();
            this.dbUnitProperties.put(dataSource, properties);
        }
        return properties;
    }

    /**
     * Sets the given {@link Properties} for all DBUnit database connections. Any
     * properties specified that have previously been set will be overridden. All
     * allowable keys are found in {@link org.dbunit.database.DatabaseConfig}. Note that
     * this method is only valid for a single-data source instance of {@link DbUnitLoader}
     * .
     *
     * @param properties The DBUnit properties.
     *
     * @throws IllegalStateException If this is not a single data source instance.
     * @throws NullPointerException If {@code properties} is {@code null}.
     *
     * @return A reference to this object.
     */
    public DbUnitLoader withProperties(Properties properties) {
        Preconditions.checkNotNull(properties, "The properties cannot be null");

        Preconditions.checkState(
            this.isSingleDataSource(),
            "Must specify a data source in multi-datasource instance");

        DbUnitLoader.logger.debug("Adding properties {}", properties);

        return this.withProperties(this.singleDataSource, properties);
    }

    /**
     * Sets the given {@link Properties} for any DBUnit database connections derived from
     * the {@link DataSource}. Any properties specified that have previously been set will
     * be overridden. All allowable keys are found in
     * {@link org.dbunit.database.DatabaseConfig}.
     *
     * @param dataSource The data source the properties are for.
     * @param properties The DBUnit properties.
     *
     * @throws IllegalStateException If this is a single data source instance or if
     *         properties for the given data source already exist.
     * @throws NullPointerException If {@code dataSource} or {@code properties} are
     *         {@code null}.
     *
     * @return A reference to this object.
     */
    public DbUnitLoader withProperties(DataSource dataSource, Properties properties) {
        Preconditions.checkNotNull(dataSource, "The data source cannot be null");
        Preconditions.checkNotNull(properties, "The properties cannot be null");

        Preconditions.checkState(
            this.isDataSourceAllowed(dataSource),
            "Cannot add second datasource to single-datasource instance");

        DbUnitLoader.logger.debug("Adding properties {}", properties);

        this.getProperties(dataSource).putAll(properties);

        return this;
    }

    /**
     * Adds another replacement key-value pair to this instance. Any occurrence of the
     * passed key in the data file will be replaced by the passed value. All keys must be
     * enclosed with brackets ({@code [key]}).
     *
     * @param key The string in the data file to replace.
     * @param value The value to replace the key with.
     *
     * @throws NullPointerException If {@code key} is {@code null}.
     *
     * @return This instance.
     */
    public DbUnitLoader addReplacementData(String key, Object value) {
        Preconditions.checkNotNull(key, "The key cannot be null");

        DbUnitLoader.logger.debug("Adding replacement data {} for key {}", value, key);

        this.replacementData.put(key, value);

        return this;
    }

    /**
     * Adds default replacement data for {@code [SYSDATE]} and {@code [USER]} literals.
     *
     * @return This instance.
     */
    public DbUnitLoader withDefaultReplacementData() {
        DbUnitLoader.logger.debug("Setting up defeault replacement strings");

        return this
                .addReplacementData("[SYSDATE]", Timestamp.valueOf(LocalDateTime.now()))
                .addReplacementData("[USER]", "test_user");
    }

    /**
     * Retrieves the current DBUnit replacement data for the give {@code key}.
     *
     * @param key The key of the replacement data.
     *
     * @return The replacement data.
     */
    public Object getReplacementData(String key) {
        DbUnitLoader.logger.debug("Retrieving replacement data for key {}", key);

        Object object = this.replacementData.get(key);

        DbUnitLoader.logger.debug(
            "Retrieved replacement data for key {}: {}",
            key,
            object);
        return object;
    }

    /**
     * Loads the database with the configured options of this {@link DbUnitLoader}.
     *
     * @return This instance.
     */
    public DbUnitLoader load() {
        DbUnitLoader.logger.info("Loading datasets with configured options");
        DbUnitLoader.logger.info("Replacement data: {}", this.replacementData);

        try {
            for (DataSource dataSource : this.dataSets.keySet()) {
                Connection connection = null;
                try {
                    connection = DataSourceUtils.getConnection(dataSource);

                    try {
                        DbUnitLoader.logger.info(
                            "Loading datasets for database {}",
                            connection.getMetaData().getURL());
                    } catch (SQLException sQLE) {
                        // Ignore exception thrown during logging
                    }
                    DbUnitLoader.logger.info(
                        "Using configured properties: {}",
                        this.dbUnitProperties.get(dataSource));

                    for (SchemaDataSet schemaDataSet : this.dataSets.get(dataSource)) {
                        DbUnitLoader.logger.info(
                            "Loading dataset for schema {}",
                            schemaDataSet.getSchema());

                        this.loadSchema(dataSource, connection, schemaDataSet);
                    }
                } finally {
                    DataSourceUtils.releaseConnection(connection, dataSource);
                }
            }
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }

        DbUnitLoader.logger.info("Database load complete");

        return this;
    }

    private void loadSchema(
            DataSource dataSource,
            Connection connection,
            SchemaDataSet schemaDataSet)
            throws DatabaseUnitException, SQLException {
        // Note that closing the IDatabaseConnection closed the underlying JDBC
        // connection
        // So DON'T DO IT!!!!
        IDatabaseConnection iDatabaseConnection = DbUnitLoader.createDbUnitConnection(
            connection,
            schemaDataSet.getSchema());

        iDatabaseConnection.getConfig().setPropertiesByString(
            this.getProperties(dataSource));

        IDataSet replacementDataSet = this.replaceDataSet(schemaDataSet.getDataSet());
        schemaDataSet.getDatabaseOperation().execute(
            iDatabaseConnection,
            replacementDataSet);
    }

    private static IDatabaseConnection createDbUnitConnection(
            Connection connection,
            String schema)
            throws DatabaseUnitException {
        try {
            DbUnitLoader.logger.debug(
                "Creating DBUnit connection on connection to database {} with schema {}",
                connection.getMetaData().getURL(),
                schema);
        } catch (SQLException sQLE) {
            // Ignore exception thrown during logging
        }

        IDatabaseConnection databaseConnection = new DatabaseConnection(
            connection,
            schema);
        return databaseConnection;
    }

    private IDataSet replaceDataSet(IDataSet dataSet) {
        DbUnitLoader.logger.debug("Replacing data in dataSet");

        ReplacementDataSet replacementDataSet = new ReplacementDataSet(dataSet);
        replacementDataSet.setStrictReplacement(true);

        for (String key : this.replacementData.keySet()) {
            replacementDataSet.addReplacementObject(key, this.replacementData.get(key));
        }
        return replacementDataSet;
    }

    private class SchemaDataSet {
        private String schema;
        private IDataSet dataSet;
        private DatabaseOperation databaseOperation;

        public SchemaDataSet(
                String schema,
                IDataSet dataSet,
                DatabaseOperation databaseOperation) {
            this.schema = schema;
            this.dataSet = dataSet;
            this.databaseOperation = databaseOperation;
        }

        public String getSchema() {
            return this.schema;
        }

        public IDataSet getDataSet() {
            return this.dataSet;
        }

        public DatabaseOperation getDatabaseOperation() {
            return this.databaseOperation;
        }
    }
}
